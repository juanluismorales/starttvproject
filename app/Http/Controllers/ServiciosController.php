<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
use App\Servicios;


class ServiciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function nameExist($name) {
        $client_name = strtoupper($name);

        $servicios = Servicio::all();
        foreach ($servicios as $Servicio) {
            $cliente = strtoupper($Servicio->nombre_cliente);
            if($cliente == $client_name) {
                return 'existe';
            }
        }
        return 'no existe';
    }



    public function index()
    {
        $clientes =Servicios::all();
        return view('servicios.alta', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { $clientes =Servicios::all();
        return view('servicios.alta', compact('clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Servicio = new Servicio();
        $Servicio->nombre_cliente =$request->input('nombre_cliente');
        $Servicio->tel_local =$request->input('tel_local');
        $Servicio->tel_celular =$request->input('tel_celular');
        $Servicio->nombre_contacto =$request->input('nombre_contacto');
        $Servicio->servicio_tecnico =$request->input('servicio_tecnico');
        $Servicio->televisiones =$request->input('televisiones');
        $Servicio->comentarios =$request->input('comentarios');
        $Servicio->domicilio =$request->input('domicilio');
        $Servicio->save();
        return response()->json($Servicio);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
