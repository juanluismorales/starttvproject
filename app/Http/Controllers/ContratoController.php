<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
// Models
use App\Contrato;


class ContratoController extends Controller
{
    /**
     * Get all packs from DataBase
     */
    private function getPaquetes() {
        $packs = DB::select('select * from paquete');
        return $packs;
    }

    public function nameExist($name) {
        $client_name = strtoupper($name);

        $contratos = Contrato::all();
        foreach ($contratos as $contrato) {
            $cliente = strtoupper($contrato->nombre_cliente);
            if($cliente == $client_name) {
                return 'existe';
            }
        }
        return 'no existe';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = array();
        $config['center'] = 'auto';
        $config['map_width'] = 400;
        $config['map_height'] = 400;
        $config['zoom'] = 15;
        $config['onboundschanged'] = 'if (!centreGot) {
            var mapCentre = map.getCenter();
            marker_0.setOptions({
                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())

            });
        }
        centreGot = true;';

        \Gmaps::initialize($config);

        // Colocar el marcador
        // Una vez se conozca la posición del usuario
        $marker = array();
        \Gmaps::add_marker($marker);

        $map = \Gmaps::create_map();

        //Devolver vista con datos del mapa
        return view('main.map', compact('map'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // direccionamos a nuestra vista para alta de contrato
        $packs = $this->getPaquetes();
        $clients = Contrato::all();
        return view('contratos.alta', compact(['packs', 'clients']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contrato = new Contrato();
        $contrato->nombre_cliente = $request->input('nombre_cliente');
        $contrato->domicilio = $request->input('domicilio');
        $contrato->tel_local = $request->input('tel_local');
        $contrato->tel_cel = $request->input('tel_cel');
        $contrato->nombre_contacto = $request->input('nombre_contacto');
        $contrato->medio = $request->input('medio');
        $contrato->id_paquete = $request->input('id_paquete');
        $contrato->televisiones = $request->input('televisiones');
        $contrato->save();
        return redirect('/contrato') -> with('REGISTRO EXITOSO');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function busqueda(){
        return view('contratos.buscar');

    }
}
