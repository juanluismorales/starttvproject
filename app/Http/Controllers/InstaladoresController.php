<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Instalador;


class InstaladoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Instaladores = Instalador::orderBy ( 'id' , 'DESC' )->paginate ( 3 );

       return view( 'Instaladores.index' ,compact ( 'Instaladores' ));
    }

    /**3
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Instaladores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,(['nombre'=>'required','telefono'=>'required']));
        main::create($request->all());
        return redirect()->route('Instaladores.index')->with('Registro exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Instaladores=instalador::find($id);
        return view('Instaladores.show', compact('Instaladores'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Instaladores=instalador::find($id);
        return view('Instaladores.show', compact('Instaladores'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'folio'=>'required', 'nombre'=>'required', 'apellido'=>'required', 'telefono'=>'required']);

        instalador::find($id)->update($request->all());
        return redirect()->route('Instaladores.index')->with('Registro actualizado satisfactoriamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        instalador::find($id)->delete();
        return redirect()->route('Instaladores.index')->with('Registro eliminado satisfactoriamente');
    }
}
