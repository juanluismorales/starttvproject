<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instalador extends Model
{
    //
    protected $fillable = ['folio', 'nombre', 'apellido', 'telefono'];

}
