<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main.index1');
});
Route::get('/mapa', function(){
    $config = array();
    $config['center'] = 'New York, USA';
    GMaps::initialize($config);
    $map = GMaps::create_map();

    echo $map['js'];
    echo $map['html'];
});
Route::resource('/contrato', 'ContratoController');
Route::get('/packs', 'ContratoController@getPaquetes');
Route::get('/name/{name}', 'ContratoController@nameExist');
Route::get('/busqueda', 'ContratoController@busqueda');



Route::resource('/servicios', 'ServiciosController');
Route::get('/name/{name}', 'ServiciosController@nameExist');


Route::resource('/cita', 'CitaController');



Route::resource('/reportes','ContratoController@reportes');


Route::get('reportes',  function (){
	return view('contratos.reportes');
});




Route::resource('/agendar', 'AgendarSerController');



Route::get('reporte',  function (){
	return view('servicios.reporte');
});

Route::get('buscar', function(){
	return view('servicios.buscar');
});






Route::resource('/servicios', 'ServiciosController');

Route::resource('/Instaladores', 'InstaladoresController');


