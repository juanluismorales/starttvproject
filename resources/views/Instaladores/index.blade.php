
@extends('main.app', ['titulo' => 'Instaladores'])

@section('content')
<div class="row">
  <section class="content">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-body">
        
          <div class="pull-right">
            <div class="btn-group">
              <a href="{{ route('Instaladores.create') }}" class="btn btn-info" >Registrar</a>
            </div>
          </div>
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>FOLIO</th>
               <th>NOMBRE</th>
               <th>APELLIDO</th>
               <th>TELEFONO</th>
               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($Instaladores->count())  
              @foreach($Instaladores as $ins)
              {{$ins}}
              <tr>
                <td>{{$instalador->folio}}</td>
                <td>{{$instalador->nombre}}</td>
                <td>{{$instalador->apellido}}</td>
                <td>{{$instalador->telefono}}</td>
                
                <td><a class="btn btn-primary btn-xs" href="{{action('InstaladoresController@edit', $instalador->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('InstaladoresController@destroy', $instalador->id)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">
 
                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
            </tbody>
 
          </table>
        </div>
      </div>

      {{ $Instaladores->links() }}
    </div>
  </div>
</section>
 
@endsection