


@extends('main.app', ['titulo' => 'Alta de Servicio'])

@section('content')
<br>
<br>

    <form class="form-group" method="POST" action="/servicios">
        @csrf
        @include('servicios.formAlta', ['clientes'=> $clientes])
        <div class="row">
            <div class="col-sm-12" id=mensaje_servicios>
            </div>
            <div class="col-sm-12 text-center">
                <button type="submit" class="btn btn-outline-primary" name="boton" id="btn-enviar"  value="insertar">
                    Registrar
                </button>
            </div>
        </div>
        
    </form>
@endsection
