@extends('main.app', ['titulo' => 'Agendar Servicio'])
@section('content')

<br>
<br>

<div id="jsGrid"></div>


<script>
     var clients = [
        { "hora": "8:00 - 8:30" },
        { "hora": "9:00 - 9:30" },
        { "hora": "10:00 - 10:30" },
        { "hora": "11:00 - 11:30" },
        { "hora": "12:00 - 12:30" },
        { "hora": "1:00 - 1:30" },
        { "hora": "2:00 - 2:30" },
        { "hora": "3:00 - 3:30" },
        { "hora": "4:00 - 4:30"},




    ];


    var instaladores_ = [];
    var clientes_ = [];

    instaladores_ = <?php echo json_encode($instaladores); ?>;
    clientes_ = <?php echo json_encode($clientes); ?>;


 console.log(instaladores_);

   
 
    var countries = [
        { Name: "", Id: 0 },
        { Name: "United States", Id: 1 },
        { Name: "Canada", Id: 2 },
        { Name: "United Kingdom", Id: 3 }
    ];
 
   $("#jsGrid").jsGrid({
        width: "100%",
        height: "400px",

        inserting: true,
        editing: true,
        paging: true,
        confirmDeleting: true,
        deleteConfirm: "Are you sure?",
        data: clientes_,

 
        fields: [
            { name: "nombre_cliente", type: "text", width: 150, validate: "required" },
            { name: "Domicilio", type: "text", width: 100 },
            { name: "Fecha de instalación", type: "text", width: 200 },
            { name: "Instalador", type: "select", items: countries, valueField: "Id", textField: "Name" },
             { name: "Horario de Instalación", type: "select", items: clients, valueField: "hora", textField: "hora" },
            { type: "control" }
        ]
    });
</script>
 







@endsection


