@extends('main.app', ['titulo' => 'Reporte de Servicios'])
@section('content')

<br>
<br>

<br>

<div id="jsGrid"></div>


<script>
    var clients = [
        { "Name": "Otto Clay", "Age": 25, "Country": 1, "Address": "Ap #897-1459 Quam Avenue", "Married": false },
        { "Name": "Connor Johnston", "Age": 45, "Country": 2, "Address": "Ap #370-4647 Dis Av.", "Married": true },
        { "Name": "Lacey Hess", "Age": 29, "Country": 3, "Address": "Ap #365-8835 Integer St.", "Married": false },
        { "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
        { "Name": "Ramona Benton", "Age": 32, "Country": 3, "Address": "Ap #614-689 Vehicula Street", "Married": false }
    ];
 
    var countries = [
        { Name: "", Id: 0 },
        { Name: "United States", Id: 1 },
        { Name: "Canada", Id: 2 },
        { Name: "United Kingdom", Id: 3 }
    ];
 
    $("#jsGrid").jsGrid({
        width: "100%",
        height: "400px",
 
        inserting: true,
        editing: true,
        sorting: true,
        paging: true,
    
 
        data: clients,
 
         fields: [
            { name: "ID", type: "text", width: 50, validate: "required" },
            { name: "Nombre de instalador", type: "number", width: 90 },
            { name: "Total de citas", type: "text", width: 90 },
            { name: "Citas confirmadas", type: "select", items: countries, valueField: "Id", textField: "Name" },
            { name: "Citas pendientes", type: "select", items: countries, valueField: "Id", textField: "Name" },
            { name: "Citas reagendadas", type: "select", items: countries, valueField: "Id", textField: "Name" },
            { name: "Productividad", type: "select", items: countries, valueField: "Id", textField: "Name" },
            { name: "Meta", type: "select", items: countries, valueField: "Id", textField: "Name" },
            { name: "Meta semanal alcanzada", type: "select", items: countries, valueField: "Id", textField: "Name" },
            { name: "Total semanal agendado", type: "select", items: countries, valueField: "Id", textField: "Name" },
            { type: "control" }
        ]
    });
</script>



@endsection
