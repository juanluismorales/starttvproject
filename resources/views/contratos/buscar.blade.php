@extends('main.app', ['titulo' => 'Buscar Contrato'])
@section('content')


<br>
<br>
 <div class="container h-100">
    

      <div class="d-flex justify-content-center h-100">
        <div class="searchbar">
          <input class="search_input" type="text" name="" placeholder="Search...">
          <a href="" class="search_icon"><i class="fas fa-search"></i></a>
        </div>
      </div>



    </div>

<br>
<!------------------------------------------>
<div id="jsGrid"></div>


<script>
    var clients = [
        { "Name": "Otto Clay", "Age": 25, "Country": 1, "Address": "Ap #897-1459 Quam Avenue", "Married": false },
        { "Name": "Connor Johnston", "Age": 45, "Country": 2, "Address": "Ap #370-4647 Dis Av.", "Married": true },
        { "Name": "Lacey Hess", "Age": 29, "Country": 3, "Address": "Ap #365-8835 Integer St.", "Married": false },
        { "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
        { "Name": "Ramona Benton", "Age": 32, "Country": 3, "Address": "Ap #614-689 Vehicula Street", "Married": false }
    ];
 
    var countries = [
        { Name: "", Id: 0 },
        { Name: "United States", Id: 1 },
        { Name: "Canada", Id: 2 },
        { Name: "United Kingdom", Id: 3 }
    ];
 
    $("#jsGrid").jsGrid({
        width: "100%",
        height: "400px",
 
        inserting: true,
        editing: true,
        sorting: true,
        paging: true,
 
        data: clients,
 
        fields: [
            { name: "NOMBRE DE CLIENTE", type: "text", width: 150, validate: "required" },
            { name: "NUMERO DE TELEFONO", type: "text", width: 100 },
            { name: "DIRECCION", type: "text", width: 160 },
            { name: "Fecha de Instalación", type: "text", width: 160 },
          
            { type: "control" }
        ]
    });
</script>
 







@endsection


