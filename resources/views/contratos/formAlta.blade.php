

<div class="row ">
        <!-- Columna prinicpal 1  -->
        <div class="col-sm-6">
            <div class="row">
                 <div class="col-sm-12">
                     <div class="form-group">
                        <label for="nombre_cliente"> Nombre del Cliente </label>
                        <input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" onchange="event => nameMatch(event)" required>
                    </div>
                 </div>
                 <div class="col-sm-6">
                     <div class="form-group">
                        <label for="telefono_local"> Teléfono Local </label>
                        <input type="text" class="form-control" id="telefono_local" name="tel_local" required>
                    </div>
                 </div>
                 <div class="col-sm-6">
                     <div class="form-group">
                        <label for="telefono_cel_1"> Teléfono Celular </label>
                        <input type="text" class="form-control" id="telefono_cel_1" name="tel_cel" required>
                    </div>
                 </div>

                 <div class="col-sm-12">
                     <div class="form-group">
                        <label for="nombre_contacto"> Nombre del Contacto </label>
                        <input type="text" class="form-control" id="nombre_contacto" name="nombre_contacto" required>
                    </div>
                 </div>
                 <div class="col-sm-6">
                     <div class="form-group">
                        <label for="id_paquete"> Paquete </label>
                        <select name="id_paquete" id="id_paquete" class="form-control" required>
                            @foreach ($packs as $pack)
                                <option value={{$pack->id}}>{{$pack->nombre}}</option>
                            @endforeach
                        </select>   
                    </div>
                        <div class="col-sm-12">
                        <label for="recomendacion">Como se entero del servicio</label>
                        <select name="medio" class="form-control">
                            <option value="Radio">Radio</option>
                            <option value="Recomendación">Recomendación</option>
                            <option value="Volante">Volante</option>
                            <option value="Perifoneo">Perifoneo</option>
                            <option value="Periodico">Periodico</option>
                            <option value="Facebook">Facebook</option>
                        </select>
                    </div>
                 </div>

                 <div class="col-sm-6">
                     <div class="form-group">
                        <label for="num_televisores"> Numero de televisores </label>
                        <input type="number" class="form-control" id="num_televisores" name="televisiones" required>
                    </div>
                    <div class="col-sm-12">
                     <label for="comentario"> Comentario </label>
                    <textarea name="comentario" cols="300" rows="5" class="form-control" id="comentario"></textarea>
                    </div>

                 </div>
             </div>

        </div>
        <!-- Termina columna prinicpal 1  -->
        <!-- Columna prinicpal 2  -->
        <div class="col-sm-6">
             <div class="col-sm-12">
                     <label for="domicilio"> Domicilio </label>
                    <input type="text" class="form-control" id="domicilio" name="domicilio" required>
                    </div>
                 <div class="col-sm-12">
                     <div class="map_canvas"></div>

                    <div class="input-group" style="margin-top: 10px">
                        <input id="geocomplete" class="form-control" type="text" placeholder="Type in an address" value="Aguascalientes, Ags., México" />
                        <span class="input-group-btn">
                            <input id="find" type="button" class="btn btn-default " value="Buscar" />
                        </span>
                    </div>
                    <p>
                          <a id="reset" class="hidden" href="#" >Reiniciar mapa</a>
                      </p>

                 </div>
            </div>
        </div>
        <!-- Termina columna prinicpal 2  -->
    </div>

    <script>
            nameMatch = (event) => {
                console.log('yeah');
            }
        </script>
        
