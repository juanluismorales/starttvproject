

@extends('main.app', ['titulo' => 'Alta de Contrato'])

@section('content')
<br>
<br>
    <form class="form-group" method="POST" action="/contrato">
        @csrf
        @include('contratos.formAlta', ['packs'=> $packs, 'clients'=> $clients])
        <div class="row">
            <div class="col-sm-12" id=mensaje_contrato>
            </div>
            <div class="col-sm-12 text-center">
                <button type="submit" class="btn btn-outline-primary" name="boton" id="btn-enviar"  value="insertar">
                    Registrar
                </button>
            </div>
        </div>
        
    </form>
@endsection
