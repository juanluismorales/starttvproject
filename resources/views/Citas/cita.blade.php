@extends('main.app', ['titulo' => 'Agendar Cita'])
@section('content')



<br>
<div id="jsGrid"></div>


<script>

    var clients = [
        { "hora": "8:00 - 8:30" },
        { "hora": "9:00 - 9:30" },
        { "hora": "10:00 - 10:30" },
        { "hora": "11:00 - 11:30" },
        { "hora": "12:00 - 12:30" },
        { "hora": "1:00 - 1:30" },
        { "hora": "2:00 - 2:30" },
        { "hora": "3:00 - 3:30" },
        { "hora": "4:00 - 4:30"},




    ];

    var instaladores_ = [];
    var clientes_ = [];

    instaladores_ = <?php echo json_encode($instaladores); ?>;
    clientes_ = <?php echo json_encode($clientes); ?>;


   /*  var todo = () => {
        var n = [];
        clientes_.forEach(i => {
            n.
        })
    } */

    console.log(instaladores_);

    $("#jsGrid").jsGrid({
        width: "100%",
        height: "400px",

        inserting: true,
        editing: true,
        paging: true,
        confirmDeleting: true,
        deleteConfirm: "Are you sure?",
        data: clientes_,


        fields: [
            { name: "nombre_cliente", type: "text", width: 150, validate: "required",  sorting: true,                                 // disable sorting for column
            },
            { name: "Domicilio", type: "text", width: 100 },
            { name: "Fecha de Instalación", type: "text", width: 200 },
            { name: "Instalador", type: "select", items: instaladores_, valueField: "id", textField: "nombre" },
            { name: "Horario de Instalación", type: "select", items: clients, valueField: "hora", textField: "hora" },
            {
                type: 'control',
                editButton: false,
                deleteButton: false,
                insertButton: true
            },
            {
                type: "control",
                editButton: false,                               // show edit button
                deleteButton: true,                             // show delete button
                clearFilterButton: true,                        // show clear filter button
                modeSwitchButton: true,
                deleteButtonTooltip: "Eliminar",
            },
            {
                type: "control",
                editButton: true,                               // show edit button
                deleteButton: false,                             // show delete button
                clearFilterButton: true,                        // show clear filter button
                modeSwitchButton: true,
                editing: false,                                 // disable editing for column
                editButtonTooltip: "Editar",
            }
        ]
    });




    </script>


@endsection