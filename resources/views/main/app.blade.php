<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/app.css" type="text/css">
    <title>StartTV</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>



<!-------------------Busqueda---------------------------->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<!---------------------------->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
  <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>

  <!------------------------------------busqueda---------------------------------------->
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>

<!----------------------------->
   <link rel="stylesheet" type="text/css" href="libs/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css">
        <script src="libs/bootstrap-datetimepicker-master/build/js/moment-with-locales.min.js" type="text/javascript"></script>
        <script src="libs/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        
        <!-- Datatable CSS -->
        <link rel="stylesheet" type="text/css" href="libs/DataTables-1.10.11/css/dataTables.bootstrap.min.css"/>

        <!-- Datatable JS -->
        <script type="text/javascript" src="libs/DataTables-1.10.11/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="libs/DataTables-1.10.11/js/dataTables.bootstrap.min.js"></script>

        <script type="text/javascript" src="libs/html2canvas.js"></script>
        <!-- Google Maps
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;key=AIzaSyCEnJihmCd3ZPKPfJBlJor8I8Yf9hDJbkY"></script>-->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;key=AIzaSyCEnJihmCd3ZPKPfJBlJor8I8Yf9hDJbkY"></script>
        <!-- gmap3 -->
        <script src="libs/gmap3.min.js" type="text/javascript"></script>

        <!-- TableTools -->
        <script src="libs/DataTables-1.10.11/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="libs/DataTables-1.10.11/extensions/TableTools/css/dataTables.tableTools.min.css">

        
        <style type="text/css" media="screen">
            #btn-enviar{
                color: black;
                background-color: #F5B644;
                border: none;
                width: 120px;
                height: 30px;
                margin-top: 20px;
                font-size: 17;
            }

            #btn-consultar{
                color: black;
                background-color: #F5B644;
                border: none;
                width: 120px;
                height: 30px;
                margin-top: 20px;
                font-size: 17;
                }
            }

            #form{
                margin-top: 20px;
            }
            #map_canvas{
                width: 100%;
                height: 520px;
            }
            
            #instaladores img{
                padding: 0 5px;
            }
            #instaladores span{
                margin-right: 25px;
            }
            #tabla_contratos_filter{
                margin-top:10px;
            }

            .servivioscolor2 {
                color: #151760;
                    }
        </style>
        <style type="text/css" media="print">
            #contenedor_tabla_contratos td{
                font-size: 10px;
            }
            #contenedor_tabla_contratos th{
                font-size: 8px;
            }
            #contenedor_tabla_contratos td{
                padding: 2px;
            }
        </style>
        <style type="text/css" >
            #map_canvas.active {
                width:  640px !important;
                height: 480px !important;
                margin: .4rem;;
            }
            @media print {
                img {
                    max-width: auto !important;
                }
                body {
                    margin: 8px;
                }
                
            }
        </style>
        <script type="text/javascript">
            var id_contrato = 0;
            var nombre = "";

            var html_cargando   =   '<div class="progress progress-big">'+
                                      '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                                        'Cargando...<span class="sr-only">100% Complete</span>'+
                                      '</div>'+
                                    '</div>';

            var html_procesando =   '<div class="progress progress-big">'+
                                      '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                                        'Procesando...<span class="sr-only">100% Complete</span>'+
                                      '</div>'+
                                    '</div>';

            var html_eliminar   =   '<div class="progress progress-big">'+
                                      '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                                        'Eliminando...<span class="sr-only">100% Complete</span>'+
                                      '</div>'+
                                    '</div>';

            $(document).ready(function() {
                $('#menu_operaciones').addClass('active');
                $('#menu_reporte').addClass('active');

                $('.dtps').datetimepicker({
                    //viewMode: 'years',
                    format: 'DD/MM/YYYY',
                    locale:"es",
                    defaultDate: Date(),
                    //useCurrent: true
                }).on("dp.error", function(){
                    alert("La fecha introducida es incorrecta");
                }).on("dp.change", function(){
                    consulta_tabla();
                    consulta_mapa();
                });





                

            });//TERMINA READY

            function cambia_instalador(){
                consulta_tabla();
                consulta_mapa();
            }

            function consulta_tabla(){
                $('#contenedor_tabla_contratos').html(html_cargando);
                //se consulta la tabla de contratos sin asignar
                var fecha_instalacion   = $('#fecha_instalacion').val();
                var id_instalador       = $('#id_instalador').val();

                $.post('tablas/tabla_contratos_agendados_reporte.php',{fecha_instalacion:fecha_instalacion,id_instalador:id_instalador}, function(data, textStatus, xhr) {
                    $('#contenedor_tabla_contratos').html(data);

                    $('.ver_cita').click(function(event) {
                        id_contrato = $(this).parent().parent().data("id");
                        nombre = $(this).parent().parent().data("nombre");


                        //se consultan los datos de la cita y se presenta el resultado en la columna derecha
                    });

                    $('.reagendar_cita').click(function(event) {
                        id_contrato = $(this).parent().parent().data("id");
                        $.post('php/registrar/registrar_historico.php', {id_contrato: id_contrato}, function(data, textStatus, xhr) {
                            if(data.resultado == "1"){
                                consulta_tabla();
                            }else{
                                alert(data.mensaje);
                            }
                        },'json').fail(function(){
                            alert("Error en la comunicación, verifique su conexión a Internet.");
                        }); 

                    });

                    $('#tabla_contratos').DataTable({
                        "dom": ' <"clear"> T lfrtip  ',
                        tableTools: {
                            "sSwfPath": "http://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf"
                        },
                        "order": [],
                        "pageLength": 100,
                        "language": {
                            "sProcessing":     "Procesando...",
                            "sLengthMenu":     "Mostrar _MENU_ registros",
                            "sZeroRecords":    "No se encontraron resultados",
                            "sEmptyTable":     "Ningún dato disponible en esta tabla",
                            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix":    "",
                            "sSearch":         "Buscar:",
                            "sUrl":            "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst":    "Primero",
                                "sLast":     "Último",
                                "sNext":     "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        }
                    });

                }).fail(function(){
                    $('#contenedor_tabla_contratos').html("<div class='alert alert-danger'> Error en la comunicación, verifique su conexión a Internet.</div>");
                });


            }

            function consulta_mapa(){

                var fecha_instalacion  = $('#fecha_instalacion').val();
                var id_instalador       = $('#id_instalador').val();

                var dia = ( fecha_instalacion.substr(0,2) );
                var mes = ( fecha_instalacion.substr(3,2) );
                var anio = ( fecha_instalacion.substr(6,4) );

                fecha_instalacion = anio+"-"+mes+"-"+dia;

                switch(mes){
                    case "01": mes = "enero";       break;
                    case "02": mes = "febrero";     break;
                    case "03": mes = "marzo";       break;
                    case "04": mes = "abril";       break;
                    case "05": mes = "mayo";        break;
                    case "06": mes = "junio";       break;
                    case "07": mes = "julio";       break;
                    case "08": mes = "agosto";      break;
                    case "09": mes = "septiembre";  break;
                    case "10": mes = "octubre";     break;
                    case "11": mes = "noviembre";   break;
                    case "12": mes = "diciembre";   break;
                }

                $('#span_fecha_instalacion').html(dia+" de "+mes+" de "+anio);

                $('#map_canvas').html(html_cargando);

                $('#contenedor_columna_2').removeClass('hidden');

                //se consulta los puntos de la instalacion
                $.post('php/consultar/consultar_contratos_agendados_reporte.php',{fecha_instalacion: fecha_instalacion, id_instalador:id_instalador}, function(data, textStatus, xhr) {
                    $('#instaladores').html(data.instaladores);

                    console.dir(data);
                    $('#map_canvas').gmap3('destroy');
                    $('#map_canvas').gmap3({
                      map:{
                        options:{
                          center:[data.x,data.y],
                          zoom: 11
                        }
                      },
                      marker:{
                        values: data.markas,
                        options:{
                          draggable: false
                        },
                        events:{
                          mouseover: function(marker, event, context){
                            var map = $(this).gmap3("get"),
                              infowindow = $(this).gmap3({get:{name:"infowindow"}});
                            if (infowindow){
                              infowindow.open(map, marker);
                              infowindow.setContent(context.id+" . "+context.data);
                            } else {
                              $(this).gmap3({
                                infowindow:{
                                  anchor:marker, 
                                  options:{content: context.id+" . "+context.data}
                                }
                              });
                            }
                          },
                          mouseout: function(){
                            var infowindow = $(this).gmap3({get:{name:"infowindow"}});
                            if (infowindow){
                              infowindow.close();
                            }
                          }
                        }
                      }
                    });
                },'json').fail(function(){
                    $('#map_canvas').html("<div class='alert alert-danger'> Error en la comunicación, verifique su conexión a Internet.</div>");
                });
            }



           

            

        </script>

  


</head>
<body>

    <!--directiva include(vista html, array de parametros)-->
    <!--El titulo tiene que ser dinamico para indicarle al usario en la seccion en la que se encuentra-->
    @include('main.header', ['titulo' => $titulo ?? 'Inicio'])

    <nav class="navbar navbar-default">
        <!-- carga la vista del menu-->
        @include('main.menu')

    </nav>

    <div class="container">
        <!--Aqui se va a renderizar la vista principal, es decir los formularios los mapas etc-->
        @yield('content')
    </div>

    <!--Carga de la vista del footer-->
    @include('main.footer')


    <script src="/js/app.js'" type="text/javascript"></script>
    <!-- Google Maps -->
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=placesAIzaSyBpZATF1F7TFEPCMxIx8nOPD0Ryi3V0BsE"></script>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;key=AIzaSyCEnJihmCd3ZPKPfJBlJor8I8Yf9hDJbkY"></script>

    <!-- Geocomplete -->
    <script src="libs/jquery.geocomplete.min.js" type="text/javascript"></script>
</body>
</html>
