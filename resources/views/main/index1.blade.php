<!DOCTYPE html>
<html>

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<title> Star TV </title>
		
		<link rel="stylesheet" href="css/estilo.css">
		<link rel="stylesheet" href="libs/bootstrap-3.3.6/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="libs/bootstrap/dist/css/bootstrap-theme.min.css">
		
		<script src="libs/jquery-1.12.4.min.js" type="text/javascript"></script>
		<script src="libs/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				$('#menu_ingresar').addClass('active');
			});
		</script>
	</head>
	<body>
		<style type="text/css" media="screen">
	
			.navbar-default{
				background-image: linear-gradient(to bottom,#F1AC30 100%, #000000 0);
			}
			.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {
			    color: black;
			    background-color: #000000;
			}

			.navbar-default .navbar-nav > .active > a{
				background-image: linear-gradient(to bottom,#F1AC30 100%,#F5B644 0);
			}

		</style>
		<div class="container-fluid">
			<div class="col-sm-2">
				<img src="assets/índice.png" alt="" class="img-responsive">
			</div>
			<div class="col-sm-8 text-center">
				<h2 class="titulo">Sistema de Gestión de Citas de Instalación</h2>
			</div>
		</div>
		<nav class="navbar navbar-default">
				<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        		<span class="sr-only">Toggle navigation</span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		      		</button>
		  		</div>
			
			
		  		
				</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
		</nav>
	
		<div class="container">
			

				<div class="row text-center">
					
				<div class="col-sm-4 col-sm-offset-4">

			

			<form  action="index.php"    method="post" >
						<div class="form-group">
						  <h3> Nombre de Usuario </h3>
						    </label>
		  <input type="text" class="form-control" id="f" name="usuario" placeholder="Usuario" required>

					  </div>
					</div>	
				</div>
				<div class="row text-center">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="form-group">
						    <label for="pass">
						    	<h3> Password </h3>
						    </label>
						    <input type="password" class="form-control" id="pass" name="pass" placeholder="Password" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4 text-center">
						<button type="submit" id="btn-ingresar" name="enviar" class="btn-ingresar">
							Ingresar
						</button>
					</div>
				</div>
			</form>
		</div>

	</body></div>
</html>