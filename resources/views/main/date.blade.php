<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<title> Star TV </title>
		
		<link rel="stylesheet" href="css/estilo.css">
		<link rel="stylesheet" href="libs/bootstrap-3.3.6/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="libs/bootstrap/dist/css/bootstrap-theme.min.css">
		
		<script src="libs/jquery-1.12.4.min.js" type="text/javascript"></script>
		
		<script src="libs/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		
		<link rel="stylesheet" type="text/css" href="libs/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css">
		<script src="libs/bootstrap-datetimepicker-master/build/js/moment-with-locales.min.js" type="text/javascript"></script>
		<script src="libs/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		
		<!-- Datatable CSS -->
		<link rel="stylesheet" type="text/css" href="libs/DataTables-1.10.11/css/dataTables.bootstrap.min.css"/>
		
		<!-- Datatable JS -->
		<script type="text/javascript" src="libs/DataTables-1.10.11/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="libs/DataTables-1.10.11/js/dataTables.bootstrap.min.js"></script>
	
		<script type="text/javascript" src="libs/html2canvas.js"></script>
		<!-- Google Maps 
		<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;key=AIzaSyCEnJihmCd3ZPKPfJBlJor8I8Yf9hDJbkY"></script>-->
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;key=AIzaSyCEnJihmCd3ZPKPfJBlJor8I8Yf9hDJbkY"></script>
		<!-- gmap3 -->
		<script src="libs/gmap3.min.js" type="text/javascript"></script>

		<!-- TableTools -->
		<script src="libs/DataTables-1.10.11/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
		<link rel="stylesheet" href="libs/DataTables-1.10.11/extensions/TableTools/css/dataTables.tableTools.min.css">

		
		<style type="text/css" media="screen">
			#btn-enviar{
				color: black;
				background-color: #F5B644;
				border: none;
				width: 120px;
				height: 30px;
				margin-top: 20px;
				font-size: 17;
			}

			#btn-consultar{
				color: black;
				background-color: #F5B644;
				border: none;
				width: 120px;
				height: 30px;
				margin-top: 20px;
				font-size: 17;
				}
			}

			#form{
				margin-top: 20px;
			}
			#map_canvas{
				width: 100%;
				height: 520px;
			}
			
			#instaladores img{
				padding: 0 5px;
			}
			#instaladores span{
				margin-right: 25px;
			}
			#tabla_contratos_filter{
				margin-top:10px;
			}

			.servivioscolor2 {
				color: #151760;
					}
		</style>
		<style type="text/css" media="print">
			#contenedor_tabla_contratos td{
				font-size: 10px;
			}
			#contenedor_tabla_contratos th{
				font-size: 8px;	
			}
			#contenedor_tabla_contratos td{
				padding: 2px;
			}
		</style>
		<style type="text/css" >
			#map_canvas.active {
			    width:  640px !important;
			    height: 480px !important;
			    margin: .4rem;;
		    }
			@media print {
			  	img {
			    	max-width: auto !important;
			  	}
			  	body {
				    margin: 8px;
				}
				
			}
		</style>
		<script type="text/javascript">
			var id_contrato = 0;
			var nombre = "";

			var html_cargando   = 	'<div class="progress progress-big">'+
									  '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
									    'Cargando...<span class="sr-only">100% Complete</span>'+
									  '</div>'+
									'</div>';

			var html_procesando = 	'<div class="progress progress-big">'+
									  '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
									    'Procesando...<span class="sr-only">100% Complete</span>'+
									  '</div>'+
									'</div>';

			var html_eliminar 	= 	'<div class="progress progress-big">'+
									  '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
									    'Eliminando...<span class="sr-only">100% Complete</span>'+
									  '</div>'+
									'</div>';

			$(document).ready(function() {
				$('#menu_operaciones').addClass('active');
				$('#menu_reporte').addClass('active');

				$('.dtps').datetimepicker({
					//viewMode: 'years',
	                format: 'DD/MM/YYYY',
	                locale:"es",
	                defaultDate: Date(),
	                //useCurrent: true
				}).on("dp.error", function(){
					alert("La fecha introducida es incorrecta");
				}).on("dp.change", function(){
					consulta_tabla();
					consulta_mapa();
				});



				

				

			});//TERMINA READY

			function cambia_instalador(){
				consulta_tabla();
				consulta_mapa();
			}

			function consulta_tabla(){
				$('#contenedor_tabla_contratos').html(html_cargando);
				//se consulta la tabla de contratos sin asignar
				var fecha_instalacion 	= $('#fecha_instalacion').val();
				var id_instalador 		= $('#id_instalador').val();

				$.post('tablas/tabla_contratos_agendados_reporte.php',{fecha_instalacion:fecha_instalacion,id_instalador:id_instalador}, function(data, textStatus, xhr) {
					$('#contenedor_tabla_contratos').html(data); 

					$('.ver_cita').click(function(event) {
						id_contrato = $(this).parent().parent().data("id");
						nombre = $(this).parent().parent().data("nombre");

						
						//se consultan los datos de la cita y se presenta el resultado en la columna derecha
					});

					$('.reagendar_cita').click(function(event) {
						id_contrato = $(this).parent().parent().data("id");
						$.post('php/registrar/registrar_historico.php', {id_contrato: id_contrato}, function(data, textStatus, xhr) {
							if(data.resultado == "1"){
								consulta_tabla();
							}else{
								alert(data.mensaje);
							}
						},'json').fail(function(){
							alert("Error en la comunicación, verifique su conexión a Internet.");
						});	

					});

					$('#tabla_contratos').DataTable({
						"dom": ' <"clear"> T lfrtip  ',
				        tableTools: {
				            "sSwfPath": "http://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf"
				        },
						"order": [],
						"pageLength": 100,
						"language": {
						    "sProcessing":     "Procesando...",
						    "sLengthMenu":     "Mostrar _MENU_ registros",
						    "sZeroRecords":    "No se encontraron resultados",
						    "sEmptyTable":     "Ningún dato disponible en esta tabla",
						    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
						    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
						    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
						    "sInfoPostFix":    "",
						    "sSearch":         "Buscar:",
						    "sUrl":            "",
						    "sInfoThousands":  ",",
						    "sLoadingRecords": "Cargando...",
						    "oPaginate": {
						        "sFirst":    "Primero",
						        "sLast":     "Último",
						        "sNext":     "Siguiente",
						        "sPrevious": "Anterior"
						    },
						    "oAria": {
						        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
						    }
						}
					});

				}).fail(function(){
					$('#contenedor_tabla_contratos').html("<div class='alert alert-danger'> Error en la comunicación, verifique su conexión a Internet.</div>");
				});


			}

			function consulta_mapa(){

				var fecha_instalacion  = $('#fecha_instalacion').val();
				var id_instalador 		= $('#id_instalador').val();

				var dia = ( fecha_instalacion.substr(0,2) );
				var mes = ( fecha_instalacion.substr(3,2) );
				var anio = ( fecha_instalacion.substr(6,4) );

				fecha_instalacion = anio+"-"+mes+"-"+dia;

				switch(mes){
					case "01": mes = "enero"; 		break;
					case "02": mes = "febrero"; 	break;
					case "03": mes = "marzo"; 		break;
					case "04": mes = "abril"; 		break;
					case "05": mes = "mayo"; 		break;
					case "06": mes = "junio"; 		break;
					case "07": mes = "julio"; 		break;
					case "08": mes = "agosto"; 		break;
					case "09": mes = "septiembre"; 	break;
					case "10": mes = "octubre"; 	break;
					case "11": mes = "noviembre"; 	break;
					case "12": mes = "diciembre"; 	break;
				}

				$('#span_fecha_instalacion').html(dia+" de "+mes+" de "+anio);

				$('#map_canvas').html(html_cargando);

				$('#contenedor_columna_2').removeClass('hidden');

				//se consulta los puntos de la instalacion
				$.post('php/consultar/consultar_contratos_agendados_reporte.php',{fecha_instalacion: fecha_instalacion, id_instalador:id_instalador}, function(data, textStatus, xhr) {
					$('#instaladores').html(data.instaladores);

					console.dir(data);
					$('#map_canvas').gmap3('destroy');
					$('#map_canvas').gmap3({
			          map:{
			            options:{
			              center:[data.x,data.y],
			              zoom: 11
			            }
			          },
			          marker:{
			            values: data.markas,
			            options:{
			              draggable: false
			            },
			            events:{
			              mouseover: function(marker, event, context){
			                var map = $(this).gmap3("get"),
			                  infowindow = $(this).gmap3({get:{name:"infowindow"}});
			                if (infowindow){
			                  infowindow.open(map, marker);
			                  infowindow.setContent(context.id+" . "+context.data);
			                } else {
			                  $(this).gmap3({
			                    infowindow:{
			                      anchor:marker, 
			                      options:{content: context.id+" . "+context.data}
			                    }
			                  });
			                }
			              },
			              mouseout: function(){
			                var infowindow = $(this).gmap3({get:{name:"infowindow"}});
			                if (infowindow){
			                  infowindow.close();
			                }
			              }
			            }
			          }
			        }); 
				},'json').fail(function(){
					$('#map_canvas').html("<div class='alert alert-danger'> Error en la comunicación, verifique su conexión a Internet.</div>");
				});				
			}

			function imprimir_pagina(){
				$('#map_canvas').addClass('hidden');
				$('#contenedor_columna_2 h3').addClass('hidden');
				print();
				$('#map_canvas').removeClass('hidden');
				$('#contenedor_columna_2 h3').removeClass('hidden');

			}

			function printMaps() {
				$('#map_canvas').addClass('active');
				consulta_mapa();
				setTimeout(function(){

				    var body               = $('body');
				    var mapContainer       = $('#map_canvas');
				    var mapContainerParent = mapContainer.parent();
				    var printContainer     = $('<div>');

				    printContainer
				        .addClass('print-container')
				        .css('position', 'relative')
				        .height(mapContainer.height())
				        .append(mapContainer)
				        .prependTo(body);

				    var content = body
				        .children()
				        .not('script')
				        .not(printContainer)
				        .detach();

				    /*
				     * Needed for those who use Bootstrap 3.x, because some of
				     * its `@media print` styles ain't play nicely when printing.
				     */
				    var patchedStyle = $('<style>')
				        .attr('media', 'print')
				        .text('img { max-width: none !important; }' +
				              'a[href]:after { content: ""; }')
				        .appendTo('head');

				    window.print();

				    body.prepend(content);
				    mapContainerParent.prepend(mapContainer);

				    printContainer.remove();
				    patchedStyle.remove();
				    $('#map_canvas').removeClass('active');
				    consulta_mapa();
				},3000);
			}


			

		</script>
	</head>
	<body>
		<style type="text/css" media="screen">
	
	.navbar-default{
		background-image: linear-gradient(to bottom,#F5B644 75%,#141413 40%);
	}
	.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {
	    color: black;
	    background-color: #F1AC30;
	}

	.navbar-default .navbar-nav > .active > a{
		background-image: linear-gradient(to bottom,#F1AC30 0,#F5B644 100%);
	}

	
</style>
<div class="container-fluid">
	<div class="col-sm-2">
		<img src="assets/índice.png" alt="" class="img-responsive">
	</div>
	<div class="col-sm-8 text-center">
		<h2 class="titulo">Sistema de Gesti&oacute;n de Citas de Instalaci&oacute;n</h2>
</h2>
	</div>
</div>
<nav class="navbar navbar-default">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        		<span class="sr-only">Toggle navigation</span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
      		</button>
  		</div>
	
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  		<ul class="nav navbar-nav">
  			<li class="dropdown" id="menu_administrador"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contratos <span class="caret"></span></a>
	        	<ul class="dropdown-menu">
	        	  <li id="menu_alta"><a href="alta_contrato.php">Alta de Contrato</a></li>
	        	  <li id="menu_agenda"><a href="contratos_sin_agendar.php">Agendar Cita</a></li>
	        	  <li id="menu_confirma"><a href="reporte_citas.php">Reporte de Citas</a></li>
	        	  <li><a href="buscar_contrato.php">Buscar Contrato</a></li>
        	  </ul>
	        </li>
            
            
            <li class="dropdown" id="menu_servicios">
	        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Servicios <span class="caret" color></span></a>
	        	<ul class="dropdown-menu">
	        		
		    		<li><a href="alta_servicio.php">Alta de Servicio</a></li>
		    		<li><a href="servicios_sin_agendar.php">Agendar Servicio</a></li>
		    		<li><a href="reporte_servicios.php">Reporte de Servicios</a></li>
		    		<li><a href="buscar_contrato3.php">Buscar Servicio</a></li>
	        	</ul>
	        </li>
            
            
            
            
            
            
            
	        <li><a href="cerrar_sesion.php">Cerrar Sesi&oacute;n</a></li>
  		</ul>
  		
  		
		</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
</nav>	
</html>