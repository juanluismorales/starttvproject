/ *
  html2canvas 0.5.0-alpha1 <http://html2canvas.hertzen.com>
  Copyright (c) 2015 Niklas von Hertzen

  Lanzado bajo licencia MIT
* /

(funci�n (ventana, documento, exportaciones, global, definir, indefinido) {

/ *!
 * @overview es6-promise - una peque�a implementaci�n de Promises / A +.
 * @ copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner y colaboradores (Conversi�n a ES6 API por Jake Archibald)
 * @license Licenciado bajo licencia MIT
 * Consulte https://raw.githubusercontent.com/jakearchibald/es6-promise/master/LICENSE
 * @version 2.0.1
 * /

(function () {function r (a, b) {n [l] = a; n [l + 1] = b; l + = 2; 2 === l && A ()} function s (a) {return "function "=== typeof a} function F () {return function () {process.nextTick (t)}} function G () {var a = 0, b = new B (t), c = document.createTextNode (" "); b.observe (c, {characterData:! 0}); return function () {c.data = a = ++ a% 2}} function H () {var a = new MessageChannel; a.port1. onmessage = t; return function () {a.port2.postMessage (0)}} function I () {return function () {setTimeout (t, 1)}} function t () {for (var a = 0; a <l; a + = 2) (0, n [a]) (n [a + 1]), n [a] = void 0, n [a + 1] = void 0;
l = 0} funci�n p () {} funci�n J (a, b, c, d) {prueba {a.call (b, c, d)} catch (e) {return e}} funci�n K (a, b , c) {r (funci�n (a) {var e =! 1, f = J (c, b, funci�n (c) {e || (e =! 0, b! == c? q (a, c ): m (a, c))}, funci�n (b) {e || (e =! 0, g (a, b))}) ;! e && f && (e =! 0, g (a, f)) }, a)} funci�n L (a, b) {1 === ba? m (a, bb): 2 === aa? g (a, bb): u (b, void 0, function (b) {q (a, b)}, funci�n (b) {g (a, b)})} funci�n q (a, b) {si (a === b) g (a, nuevo TypeError ("No se puede resolver una promesa consigo misma ")) else si (" function "=== typeof b ||" object "=== typeof b && null! == b) if (b.constructor === a.constructor) L (a,
b); else {var c; intente {c = b.then} catch (d) {v.error = d, c = v} c === v? g (a, v.error): void 0 == = c? m (a, b): s (c)? K (a, b, c): m (a, b)} si no m (a, b)} funci�n M (a) {af && a.f (ab ); x (a)} funci�n m (a, b) {void 0 === aa && (ab = b, aa = 1,0! == aelength && r (x, a))} funci�n g (a, b) { void 0 === aa && (aa = 2, ab = b, r (M, a))} funci�n u (a, b, c, d) {var e = ae, f = e.length; af = null; e [f] = b; e [f + 1] = c; e [f + 2] = d; 0 === f && a.a && r (x, a)} funci�n x (a) {var b = ae, c = aa; if (0! == b.length) {para (var d, e, f = ab, g = 0; g <b.length; g + = 3) d = b [g], e = b [ g + c], d? C (c, d, e, f): e (f); aelength = 0}} funci�n D () {this.error =
nulo} funci�n C (a, b, c, d) {var e = s (c), f, k, h, l; si (e) {prueba {f = c (d)} captura (n) {y .error = n, f = y} f === y? (l =! 0, k = f.error, f = null): h =! 0; if (b === f) {g (b, new TypeError ("Una devoluci�n de llamada de promesas no puede devolver esa misma promesa.")); return}} else f = d, h =! 0; void 0 === ba && (e && h? q (b, f): l? g ( b, k): 1 === a? m (b, f): 2 === a && g (b, f))} funci�n N (a, b) {prueba {b (funci�n (b) {q (a , b)}, funci�n (b) {g (a, b)})} catch (c) {g (a, c)}} funci�n k (a, b, c, d) {esto.n = a; this.c = new a (p, d); this.i = c; this.o (b)? (this.m = b, this.d = this.length = b.length, this.l (), 0 === this.length? M (this.c,
this.b) :( this.length = this.length || 0, this.k (), 0 === this.d && m (this.c, this.b))): g (this.c, this. p ())} funci�n h (a) {O ++; this.b = this.a = void 0; this.e = []; if (p! == a) {if (! s (a)) lanza una nueva TypeError ("Debe pasar una funci�n de resoluci�n como el primer argumento al constructor de promesa"); si (! (Esta instancia de h)) arroje un nuevo TypeError ("No se pudo construir 'Promesa': use el operador 'nuevo', este el constructor de objetos no se puede llamar como una funci�n. "); N (this, a)}} var E = Array.isArray? Array.isArray: function (a) {return" [object Array] "===
Object.prototype.toString.call (a)}, l = 0, w = "undefined"! == typeof window? Window: {}, B = w.MutationObserver || w.WebKitMutationObserver, w = "undefined"! = = typeof Uint8ClampedArray && "undefined"! == typeof importScripts && "undefined"! == typeof MessageChannel, n = Array (1E3), A; A = "undefined"! == typeof process && "[object process]" === {} .toString.call (process)? F (): B? G (): w? H (): I (); var v = nuevo D, y = new D; k.prototype.o = function (a) { devuelva E (a)}; k.prototype.p = function () {return Error ("Los M�todos de Array deben proporcionarse un Array")}; k.prototype.l =
function () {this.b = Array (this.length)}; k.prototype.k = function () {for (var a = this.length, b = this.c, c = this.m, d = 0 ; void 0 === ba && d <a; d ++) this.j (c [d], d)}; k.prototype.j = function (a, b) {var c = this.n; "objeto" == = typeof a && null! == a? a.constructor === c && void 0! == aa? (af = null, this.g (aa, b, ab)): this.q (c.resolve (a), b ) :( this.d -, this.b [b] = this.h (a))}; k.prototype.g = function (a, b, c) {var d = this.c; void 0 = == da && (this.d -, this.i && 2 === a? g (d, c): this.b [b] = this.h (c)); 0 === this.d && m (d, this.b)}; k.prototype.h = function (a) {return a};
k.prototype.q = function (a, b) {var c = this; u (a, void 0, function (a) {cg (1, b, a)}, function (a) {cg (2, b , a)})}; var O = 0; h.all = function (a, b) {return (nuevo k (this, a,! 0, b)). c}; h.race = function (a, b) {funci�n c (a) {q (e, a)} funci�n d (a) {g (e, a)} var e = nuevo esto (p, b); if (! E (a)) devuelve ( g (e, nuevo TypeError ("Debes pasar una matriz para correr.")), e); para (var f = a.length, h = 0; void 0 === ea && h <f; h ++) u (esto .resolve (a [h]), void 0, c, d); return e}; h.resolve = function (a, b) {if (a && "objeto" === tipo de un && a.constructor === esto) devuelve a; var c = nuevo este (p, b);
q (c, a); return c}; h.reject = function (a, b) {var c = new this (p, b); g (c, a); return c}; h.prototype = {constructor : h, entonces: funci�n (a, b) {var c = this.a; if (1 === c &&! a || 2 === c &&! b) devuelve esto; var d = nuevo this.constructor (p ), e = this.b; if (c) {var f = argumentos [c-1]; r (function () {C (c, d, f, e)})} else u (this, d, a , b); return d}, "catch": function (a) {return this.then (null, a)}}; var z = {Promise: h, polyfill: function () {var a; a = "undefined "! == typeof global? global:" undefined "! == typeof window && window.document? window: self;" Promise "en un &&" resolver "en
a.Prometo && "rechazar" en a.Promiso && "all" en a.Promiso && "carrera" en a.Promise && function () {var b; new a.Promise (function (a) {b = a}); return s (b )} () || (a.Promise = h)}}; "function" === typeof define && define.amd? define (function () {return z}): "undefined"! == typeof module && module.exports? module .exports = z: "undefined"! == typeof this && (this.ES6Promise = z);}). call (ventana);
si (ventana) {
    window.ES6Promise.polyfill ();
}


if (typeof (document) === "undefined" || typeof (Object.create)! == "function" || typeof (document.createElement ("canvas"). getContext)! == "function") {
    (window || module.exports) .html2canvas = function () {
        return Promise.reject ("No hay soporte de lienzo");
    };
    regreso;
}

/ *! https://mths.be/punycode v1.3.1 por @mathias * /
; (funci�n (ra�z) {

	/ ** Detectar variables libres * /
	var freeExports = tipo de exportaciones == 'objeto' && exportaciones &&
		! exports.nodeType && exports;
	var freeModule = tipo de m�dulo == 'objeto' && m�dulo &&
		! module.nodeType && module;
	var freeGlobal = typeof global == 'object' && global;
	Si (
		freeGlobal.global === freeGlobal ||
		freeGlobal.window === freeGlobal ||
		freeGlobal.self === freeGlobal
	) {
		root = freeGlobal;
	}

	/ **
	 * El objeto `punycode`.
	 * @name punycode
	 * @type Object
	 * /
	var punycode,

	/ ** Valor flotante de 32 bits con signo positivo m�s alto * /
	maxInt = 2147483647, // aka. 0x7FFFFFFF o 2 ^ 31-1

	/ ** Par�metros de la cadena de arranque * /
	base = 36,
	tMin = 1,
	tMax = 26,
	sesgo = 38,
	humedo = 700,
	initialBias = 72,
	initialN = 128, // 0x80
	delimitador = '-', // '\ x2D'

	/** Expresiones regulares */
	regexPunycode = / ^ xn - /,
	regexNonASCII = / [^ \ x20- \ x7E] /, // caracteres ASCII no imprimibles + caracteres no ASCII
	regexSeparators = / [\ x2E \ u3002 \ uFF0E \ uFF61] / g, // separadores RFC 3490

	/** Error de mensajes */
	errores = {
		'desbordamiento': 'Desbordamiento: la entrada necesita enteros m�s anchos para procesar',
		'no b�sico': 'Entrada ilegal> = 0x80 (no es un punto de c�digo b�sico)',
		'entrada inv�lida': 'entrada inv�lida'
	}

	/ ** Accesos directos de conveniencia * /
	baseMinusTMin = base - tMin,
	piso = Math.floor,
	stringFromCharCode = String.fromCharCode,

	/ ** Variable temporal * /
	llave;

	/ * -------------------------------------------------- -------------------------- * /

	/ **
	 * Una funci�n de utilidad de error gen�rico.
	 * @private
	 * @param {String} type El tipo de error.
	 * @returns {Error} Lanza un `RangeError` con el mensaje de error correspondiente.
	 * /
	error de funci�n (tipo) {
		lanzar RangeError (errores [tipo]);
	}

	/ **
	 * Una funci�n de utilidad gen�rica `Array # map`.
	 * @private
	 * @param {Array} array La matriz para iterar sobre.
	 * @param {Funci�n} devoluci�n de llamada La funci�n que se llama para cada matriz
	 * �t.
	 * @returns {Array} Una nueva matriz de valores devueltos por la funci�n de devoluci�n de llamada.
	 * /
	mapa de funciones (array, fn) {
		longitud var = array.length;
		var resultado = [];
		while (longitud--) {
			resultado [longitud] = fn (matriz [longitud]);
		}
		resultado de retorno
	}

	/ **
	 * Un simple envoltorio similar a `Array # map` para trabajar con cadenas de nombre de dominio o correo electr�nico
	 * direcciones.
	 * @private
	 * @param {String} dominio El nombre de dominio o la direcci�n de correo electr�nico.
	 * @param {Funci�n} devoluci�n de llamada La funci�n que se llama para cada
	 * personaje.
	 * @returns {Array} Una nueva cadena de caracteres devueltos por la devoluci�n de llamada
	 * funci�n.
	 * /
	funci�n mapDomain (string, fn) {
		var parts = string.split ('@');
		var resultado = '';
		if (parts.length> 1) {
			// En las direcciones de correo electr�nico, solo el nombre de dominio debe estar codificado. Salir
			// la parte local (es decir, todo hasta `@`) intacto.
			resultado = partes [0] + '@';
			cadena = partes [1];
		}
		var labels = string.split (regexSeparators);
		var encoded = map (labels, fn) .join ('.');
		resultado de retorno + codificado;
	}

	/ **
	 * Crea una matriz que contiene los puntos de c�digo num�rico de cada Unicode
	 * Car�cter en la cadena. Mientras que JavaScript usa UCS-2 internamente,
	 * esta funci�n convertir� un par de mitades sustitutas (cada una de las cuales
	 * UCS-2 se expone como caracteres separados) en un solo punto de c�digo,
	 * Coincidencia con UTF-16.
	 * @see `punycode.ucs2.encode`
	 * @see <https://mathiasbynens.be/notes/javascript-encoding>
	 * @memberOf punycode.ucs2
	 * @nombre decodificar
	 * @param {String} string La cadena de entrada de Unicode (UCS-2).
	 * @returns {Array} La nueva matriz de puntos de c�digo.
	 * /
	funci�n ucs2decode (cadena) {
		salida var = [],
		    contador = 0,
		    length = string.length,
		    valor,
		    extra;
		while (contador <longitud) {
			value = string.charCodeAt (counter ++);
			if (valor> = 0xD800 && valor <= 0xDBFF && contador <longitud) {
				// alto sustituto, y hay un siguiente personaje
				extra = string.charCodeAt (counter ++);
				if ((extra & 0xFC00) == 0xDC00) {// bajo sustituto
					output.push (((valor & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
				} else {
					// sustituto sin igual; solo anexar esta unidad de c�digo, en caso de que la pr�xima
					// unidad de c�digo es el sustituto alto de un par sustituto
					output.push (valor);
					mostrador--;
				}
			} else {
				output.push (valor);
			}
		}
		retorno de salida;
	}

	/ **
	 * Crea una cadena basada en una matriz de puntos de c�digo num�rico.
	 * @see `punycode.ucs2.decode`
	 * @memberOf punycode.ucs2
	 * @nombre codificar
	 * @param {Array} codePoints La matriz de puntos de c�digo num�rico.
	 * @returns {String} La nueva cadena Unicode (UCS-2).
	 * /
	funci�n ucs2encode (array) {
		mapa de retorno (matriz, funci�n (valor) {
			var output = '';
			si (valor> 0xFFFF) {
				valor - = 0x10000;
				output + = stringFromCharCode (value >>> 10 & 0x3FF | 0xD800);
				valor = 0xDC00 | valor & 0x3FF;
			}
			salida + = stringFromCharCode (valor);
			retorno de salida;
		}).unirse('');
	}

	/ **
	 * Convierte un punto de c�digo b�sico en un d�gito / entero.
	 * @see `digitToBasic ()`
	 * @private
	 * @param {Number} codePoint El valor del punto del c�digo num�rico b�sico.
	 * @returns {Number} El valor num�rico de un punto de c�digo b�sico (para uso en
	 * representando enteros) en el rango `0` a` base - 1`, o `base` si
	 * El punto de c�digo no representa un valor.
	 * /
	funci�n basicToDigit (codePoint) {
		si (codePoint - 48 <10) {
			c�digo de retornoPunto - 22;
		}
		si (codePoint - 65 <26) {
			c�digo de retorno Point - 65;
		}
		si (codePoint - 97 <26) {
			devuelve codePoint - 97;
		}
		base de retorno
	}

	/ **
	 * Convierte un d�gito / entero en un punto de c�digo b�sico.
	 * @see `basicToDigit ()`
	 * @private
	 * @param {N�mero} d�gito El valor num�rico de un punto de c�digo b�sico.
	 * @returns {Number} El punto de c�digo b�sico cuyo valor (cuando se usa para
	 * que representa enteros) es `d�gito ', que debe estar en el rango
	 * `0` a` base - 1`. Si `flag` es distinto de cero, la forma en may�scula es
	 * usado; De lo contrario, se utiliza la forma en min�scula. El comportamiento es indefinido.
	 * Si `flag` no es cero y` digit` no tiene may�sculas.
	 * /
	funci�n digitToBasic (digit, flag) {
		// 0..25 mapea a ASCII a..z o A..Z
		// 26..35 mapa a ASCII 0..9
		d�gito de retorno + 22 + 75 * (d�gito <26) - ((marca! = 0) << 5);
	}

	/ **
	 * Funci�n de adaptaci�n de polarizaci�n seg�n la secci�n 3.4 de RFC 3492.
	 * http://tools.ietf.org/html/rfc3492#section-3.4
	 * @private
	 * /
	funci�n adapt (delta, numPoints, firstTime) {
		var k = 0;
		delta = firstTime? piso (delta / h�medo): delta >> 1;
		delta + = piso (delta / numPoints);
		para (/ * sin inicializaci�n * /; delta> baseMinusTMin * tMax >> 1; k + = base) {
			delta = piso (delta / baseMinusTMin);
		}
		piso de retorno (k + (baseMinusTMin + 1) * delta / (delta + skew));
	}

	/ **
	 * Convierte una cadena de Punycode de s�mbolos solo ASCII en una cadena de Unicode
	 * simbolos.
	 * @memberOf punycode
	 * @param {String} input La cadena Punycode de los s�mbolos solo ASCII.
	 * @returns {String} La cadena resultante de los s�mbolos Unicode.
	 * /
	funci�n decodificaci�n (entrada) {
		// No uses UCS-2
		salida var = [],
		    inputLength = input.length,
		    afuera,
		    i = 0,
		    n = initialN,
		    sesgo = initialBias,
		    BASIC,
		    j
		    �ndice,
		    viejo
		    w
		    k
		    d�gito,
		    t
		    / ** Resultados del c�lculo en cach� * /
		    baseMinusT;

		// Manejar los puntos de c�digo b�sicos: sea `b�sico` el n�mero de c�digo de entrada
		// puntos antes del �ltimo delimitador, o `0` si no hay ninguno, entonces copie
		// El primer c�digo b�sico apunta a la salida.

		basic = input.lastIndexOf (delimitador);
		si (b�sico <0) {
			b�sico = 0;
		}

		para (j = 0; j <b�sico; ++ j) {
			// si no es un punto de c�digo b�sico
			if (input.charCodeAt (j)> = 0x80) {
				error ('no b�sico');
			}
			output.push (input.charCodeAt (j));
		}

		// Bucle de decodificaci�n principal: comience justo despu�s del �ltimo delimitador si hay alg�n c�digo b�sico
		// los puntos fueron copiados; empezar por el principio de lo contrario.

		para (�ndice = b�sico> 0? b�sico + 1: 0; �ndice <inputLength; / * sin expresi�n final * /) {

			// `index` es el �ndice del siguiente car�cter que se va a consumir.
			// Decodifica un entero de longitud variable generalizada en `delta`,
			// que se agrega a `i`. La comprobaci�n de desbordamiento es m�s f�cil
			// si aumentamos `i` a medida que avanzamos, luego restamos su inicio
			// valor al final para obtener `delta`.
			para (oldi = i, w = 1, k = base; / * sin condici�n * /; k + = base) {

				if (index> = inputLength) {
					error ('entrada inv�lida');
				}

				digit = basicToDigit (input.charCodeAt (index ++));

				if (digit> = base || digit> floor ((maxInt - i) / w)) {
					error ('desbordamiento');
				}

				i + = d�gito * w;
				t = k <= sesgo? tMin: (k> = sesgo + tMax? tMax: k - sesgo);

				si (d�gito <t) {
					descanso;
				}

				baseMinusT = base - t;
				if (w> floor (maxInt / baseMinusT)) {
					error ('desbordamiento');
				}

				w * = baseMinusT;

			}

			out = output.length + 1;
			sesgo = adaptar (i - oldi, out, oldi == 0);

			// Se supon�a que `` i` se envolv�a de `out` a` 0`,
			// incrementando `n` cada vez, as� que lo arreglaremos ahora:
			if (floor (i / out)> maxInt - n) {
				error ('desbordamiento');
			}

			n + = piso (i / out);
			i% = fuera;

			// Insertar `n` en la posici�n` i` de la salida
			output.splice (i ++, 0, n);

		}

		return ucs2encode (salida);
	}

	/ **
	 * Convierte una cadena de s�mbolos Unicode (por ejemplo, una etiqueta de nombre de dominio) en una
	 * Cadena de punycode de s�mbolos solo ASCII.
	 * @memberOf punycode
	 * @param {String} input La cadena de s�mbolos Unicode.
	 * @returns {String} La cadena de Punycode resultante de los s�mbolos solo ASCII.
	 * /
	funci�n de codificaci�n (entrada) {
		var n
		    delta,
		    Manej�CPCount,
		    basicLength,
		    parcialidad,
		    j
		    metro,
		    q
		    k
		    t
		    valor actual,
		    salida = [],
		    / ** `inputLength` mantendr� el n�mero de puntos de c�digo en` input`. * /
		    inputLength,
		    / ** Resultados del c�lculo en cach� * /
		    ManejaCPCountPlusOne,
		    baseMinusT,
		    qMinusT;

		// Convertir la entrada en UCS-2 a Unicode
		input = ucs2decode (input);

		// cachear la longitud
		inputLength = input.length;

		// Inicializar el estado
		n = inicial N;
		delta = 0;
		sesgo = initialBias;

		// Manejar los puntos de c�digo b�sicos
		para (j = 0; j <inputLength; ++ j) {
			currentValue = input [j];
			if (currentValue <0x80) {
				output.push (stringFromCharCode (currentValue));
			}
		}

		handleCPCount = basicLength = output.length;

		// `handlingCPCount` es el n�mero de puntos de c�digo que se han manejado;
		// `basicLength` es el n�mero de puntos de c�digo b�sicos.

		// Finalice la cadena b�sica, si no est� vac�a, con un delimitador
		if (basicLength) {
			output.push (delimitador);
		}

		// Bucle de codificaci�n principal:
		while (managedCPCount <inputLength) {

			// Todos los puntos de c�digo no b�sicos <n ya se han manejado. Encuentra el siguiente
			// uno m�s grande:
			para (m = maxInt, j = 0; j <inputLength; ++ j) {
				currentValue = input [j];
				if (currentValue> = n && currentValue <m) {
					m = valor actual;
				}
			}

			// Incrementa `delta 'lo suficiente para hacer avanzar el estado <n, i> del decodificador a <m, 0>,
			// pero cu�date del desbordamiento
			handlingCPCountPlusOne = handleCPCount + 1;
			if (m - n> floor ((maxInt - delta) / handlingCPCountPlusOne)) {
				error ('desbordamiento');
			}

			delta + = (m - n) * handleCPCountPlusOne;
			n = m;

			para (j = 0; j <inputLength; ++ j) {
				currentValue = input [j];

				if (currentValue <n && ++ delta> maxInt) {
					error ('desbordamiento');
				}

				if (currentValue == n) {
					// Representa delta como un entero de longitud variable generalizada
					para (q = delta, k = base; / * sin condici�n * /; k + = base) {
						t = k <= sesgo? tMin: (k> = sesgo + tMax? tMax: k - sesgo);
						si (q <t) {
							descanso;
						}
						qMinusT = q - t;
						baseMinusT = base - t;
						salida.push (
							stringFromCharCode (digitToBasic (t + qMinusT% baseMinusT, 0))
						);
						q = piso (qMinusT / baseMinusT);
					}

					output.push (stringFromCharCode (digitToBasic (q, 0)));
					bias = adapt (delta, handleCPCountPlusOne, handleCPCount == basicLength);
					delta = 0;
					++ ManagedCPCount;
				}
			}

			++ delta;
			++ n;

		}
		return output.join ('');
	}

	/ **
	 * Convierte una cadena de Punycode que representa un nombre de dominio o una direcci�n de correo electr�nico
	 * a Unicode. Solamente se convertir�n las partes de la entrada con c�digo de codificaci�n, es decir
	 * No importa si lo llamas en una cadena que ya ha sido
	 * convertido a Unicode.
	 * @memberOf punycode
	 * @param {String} ingrese el nombre de dominio o la direcci�n de correo electr�nico del Punycoded para
	 * Convertir a Unicode.
	 * @returns {String} La representaci�n Unicode del Punycode dado
	 * cuerda.
	 * /
	funci�n toUnicode (entrada) {
		devolver mapDomain (entrada, funci�n (cadena) {
			devolver regexPunycode.test (cadena)
				? decodificar (string.slice (4). toLowerCase ())
				: cuerda;
		});
	}

	/ **
	 * Convierte una cadena Unicode que representa un nombre de dominio o una direcci�n de correo electr�nico para
	 * Punycode. S�lo se convertir�n las partes no ASCII del nombre de dominio,
	 * es decir, no importa si lo llamas con un dominio que ya est� en
	 * ASCII.
	 * @memberOf punycode
	 * @param {String} input El nombre de dominio o la direcci�n de correo electr�nico a convertir, como
	 * Unicode cadena.
	 * @returns {String} La representaci�n de Punycode del nombre de dominio dado o
	 * direcci�n de correo electr�nico.
	 * /
	funci�n toASCII (entrada) {
		devolver mapDomain (entrada, funci�n (cadena) {
			devolver regexNonASCII.test (cadena)
				? 'xn--' + codificar (cadena)
				: cuerda;
		});
	}

	/ * -------------------------------------------------- -------------------------- * /

	/ ** Definir la API p�blica * /
	punycode = {
		/ **
		 * Una cadena que representa el n�mero de versi�n actual de Punycode.js.
		 * @memberOf punycode
		 * @type String
		 * /
		'versi�n': '1.3.1',
		/ **
		 * Un objeto de m�todos para convertir desde el car�cter interno de JavaScript.
		 * Representaci�n (UCS-2) a puntos de c�digo Unicode, y viceversa.
		 * @see <https://mathiasbynens.be/notes/javascript-encoding>
		 * @memberOf punycode
		 * @type Object
		 * /
		'ucs2': {
			'decodificar': ucs2decode,
			'codificar': ucs2encode
		}
		'decodificar': decodificar,
		'codificar': codificar,
		'toASCII': toASCII,
		'toUnicode': toUnicode
	};

	/ ** Expone `punycode` * /
	// Algunos optimizadores de compilaci�n de AMD, como r.js, verifican patrones de condici�n espec�ficos
	// me gusta lo siguiente:
	Si (
		typeof define == 'function' &&
		typeof define.amd == 'object' &&
		define.amd
	) {
		define ('punycode', function () {
			devolver punycode;
		});
	} else if (freeExports && freeModule) {
		if (module.exports == freeExports) {// en Node.js o RingoJS v0.8.0 +
			freeModule.exports = punycode;
		} else {// en Narwhal o RingoJS v0.7.0-
			para (clave en punycode) {
				punycode.hasOwnProperty (key) && (freeExports [key] = punycode [key]);
			}
		}
	} else {// en Rhino o en un navegador web
		root.punycode = punycode;
	}

}(esta));

var html2canvasNodeAttribute = "data-html2canvas-node";
var html2canvasCanvasCloneAttribute = "data-html2canvas-canvas-clone";
var html2canvasCanvasCloneIndex = 0;
var html2canvasCloneIndex = 0;

window.html2canvas = function (nodeList, opciones) {
    var index = html2canvasCloneIndex ++;
    opciones = opciones || {};
    if (options.logging) {
        window.html2canvas.logging = true;
        window.html2canvas.start = Date.now ();
    }

    options.async = typeof (options.async) === "undefined"? true: options.async;
    options.allowTaint = typeof (options.allowTaint) === "undefined"? false: options.allowTaint;
    options.removeContainer = typeof (options.removeContainer) === "undefined"? true: options.removeContainer;
    options.javascriptEnabled = typeof (options.javascriptEnabled) === "undefined"? falso: options.javascriptEnabled;
    options.imageTimeout = typeof (options.imageTimeout) === "undefined"? 10000: options.imageTimeout;
    options.renderer = typeof (options.renderer) === "function"? options.renderer: CanvasRenderer;
    options.strict = !! options.strict;

    if (typeof (nodeList) === "cadena") {
        if (typeof (options.proxy)! == "string") {
            return Promise.reject ("El proxy se debe usar al representar la url");
        }
        var width = options.width! = null? options.width: window.innerWidth;
        var height = options.height! = null? options.height: window.innerHeight;
        devolver loadUrlDocument (absoluteUrl (nodeList), options.proxy, document, width, height, options) .then (function (container) {
            devuelve renderWindow (container.contentWindow.document.documentElement, container, options, width, height);
        });
    }

    var node = ((nodeList === undefined)? [document.documentElement]: ((nodeList.length)? nodeList: [nodeList])) [0];
    node.setAttribute (html2canvasNodeAttribute + index, index);
    devuelve renderDocument (node.ownerDocument, options, node.ownerDocument.defaultView.innerWidth, node.ownerDocument.defaultView.innerHeight, index) .then (function (canvas) {
        if (typeof (options.onrendered) === "function") {
            log ("options.onrendered est� en desuso, html2canvas devuelve una Promesa que contiene el lienzo");
            options.onrendered (lienzo);
        }
        volver lienzo;
    });
};

window.html2canvas.punycode = this.punycode;
window.html2canvas.proxy = {};

function renderDocument (documento, opciones, windowWidth, windowHeight, html2canvasIndex) {
    devuelva createWindowClone (document, document, windowWidth, windowHeight, options, document.defaultView.pageXOffset, document.defaultView.pageYOffset) .then (function (container) {
        log ("Documento clonado");
        var attributeName = html2canvasNodeAttribute + html2canvasIndex;
        var selector = "[" + attributeName + "= '" + html2canvasIndex + "']";
        document.querySelector (selector) .removeAttribute (attributeName);
        var clonedWindow = container.contentWindow;
        var node = clonedWindow.document.querySelector (selector);
        var oncloneHandler = (typeof (options.onclone) === "function")? Promise.resolve (options.onclone (clonedWindow.document)): Promise.resolve (true);
        return oncloneHandler.then (function () {
            devuelve renderWindow (nodo, contenedor, opciones, windowWidth, windowHeight);
        });
    });
}

funci�n renderWindow (nodo, contenedor, opciones, windowWidth, windowHeight) {
    var clonedWindow = container.contentWindow;
    var support = new Support (clonedWindow.document);
    var imageLoader = new ImageLoader (opciones, soporte);
    l�mites var = getBounds (nodo);
    var width = options.type === "view"? windowWidth: documentWidth (clonedWindow.document);
    var height = options.type === "view"? windowHeight: documentHeight (clonedWindow.document);
    var renderer = new options.renderer (ancho, alto, imageLoader, opciones, documento);
    var parser = new NodeParser (nodo, procesador, soporte, imageLoader, opciones);
    devuelve parser.ready.then (function () {
        log ("Procesamiento finalizado");
        lienzo var;

        if (options.type === "view") {
            canvas = crop (renderer.canvas, {width: renderer.canvas.width, height: renderer.canvas.height, top: 0, left: 0, x: 0, y: 0});
        } else if (node ??=== clonedWindow.document.body || node === clonedWindow.document.documentElement || options.canvas! = null) {
            canvas = renderer.canvas;
        } else {
            canvas = crop (renderer.canvas, {width: options.width! = null? options.width: lines.width, height: options.height! = null? options.height: limits.height, top: lines.top, left : lines.left, x: clonedWindow.pageXOffset, y: clonedWindow.pageYOffset});
        }

        cleanupContainer (contenedor, opciones);
        volver lienzo;
    });
}

funci�n cleanupContainer (contenedor, opciones) {
    if (options.removeContainer) {
        container.parentNode.removeChild (container);
        registro ("contenedor limpiado");
    }
}

funci�n de recorte (lienzo, l�mites) {
    var croppedCanvas = document.createElement ("canvas");
    var x1 = Math.min (canvas.width - 1, Math.max (0, bound.left));
    var x2 = Math.min (canvas.width, Math.max (1, lines.left + lines.width));
    var y1 = Math.min (canvas.height - 1, Math.max (0, bound.top));
    var y2 = Math.min (canvas.height, Math.max (1, lines.top + lines.height));
    croppedCanvas.width = lines.width;
    croppedCanvas.height = lines.height;
    log ("Recorte de lienzo en:", "izquierda:", lines.left, "top:", lines.top, "width:", (x2-x1), "height:", (y2-y1));
    log ("Recorte resultante con ancho", lines.width, "and height" ,mites.height, "with x", x1, "and y", y1);
    croppedCanvas.getContext ("2d"). drawImage (lienzo, x1, y1, x2-x1, y2-y1, fronteras.x, fronteras.y, x2-x1, y2-y1);
    volver cosechadoCanvas;
}

funci�n documentwidth (doc) {
    volver Math.max (
        Math.max (doc.body.scrollWidth, doc.documentElement.scrollWidth),
        Math.max (doc.body.offsetWidth, doc.documentElement.offsetWidth),
        Math.max (doc.body.clientWidth, doc.documentElement.clientWidth)
    );
}

funci�n documentHeight (doc) {
    volver Math.max (
        Math.max (doc.body.scrollHeight, doc.documentElement.scrollHeight),
        Math.max (doc.body.offsetHeight, doc.documentElement.offsetHeight),
        Math.max (doc.body.clientHeight, doc.documentElement.clientHeight)
    );
}

funci�n smallImage () {
    devolver "datos: imagen / gif; base64, R0lGODlhAQABAIAAAAAAAP /// yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
}

funci�n isIE9 () {
    devuelva document.documentMode && document.documentMode <= 9;
}

// https://github.com/niklasvh/html2canvas/issues/503
function cloneNodeIE9 (node, javascriptEnabled) {
    var clone = node.nodeType === 3? document.createTextNode (node.nodeValue): node.cloneNode (false);

    var child = node.firstChild;
    mientras (ni�o) {
        if (javascriptEnabled === true || child.nodeType! == 1 || child.nodeName! == 'SCRIPT') {
            clone.appendChild (cloneNodeIE9 (child, javascriptEnabled));
        }
        child = child.nextSibling;
    }

    volver clon
}

Funci�n createWindowClone (ownerDocument, containerDocument, width, height, options, x, y) {
    labelCanvasElements (ownerDocument);
    var documentElement = isIE9 ()? cloneNodeIE9 (ownerDocument.documentElement, options.javascriptEnabled): ownerDocument.documentElement.cloneNode (true);
    var container = containerDocument.createElement ("iframe");

    container.className = "html2canvas-container";
    container.style.visibility = "oculto";
    container.style.position = "arreglado";
    container.style.left = "-10000px";
    container.style.top = "0px";
    container.style.border = "0";
    container.width = ancho;
    container.height = altura;
    container.scrolling = "no"; // ios no se desplazar� sin �l
    containerDocument.body.appendChild (contenedor);

    devolver nueva promesa (funci�n (resolver) {
        var documentClone = container.contentWindow.document;

        cloneNodeValues ??(ownerDocument.documentElement, documentElement, "textarea");
        cloneNodeValues ??(ownerDocument.documentElement, documentElement, "select");

        / * Chrome no detecta im�genes de fondo relativas asignadas en hojas <style> en l�nea cuando se obtiene a trav�s de getComputedStyle
         Si la URL de la ventana se trata de: espacio en blanco, podemos asignar la URL a la actual escribiendo en el documento.
         * /
        container.contentWindow.onload = container.onload = function () {
            intervalo de var = setInterval (function () {
                if (documentClone.body.childNodes.length> 0) {
                    cloneCanvasContents (ownerDocument, documentClone);
                    clearInterval (intervalo);
                    if (options.type === "view") {
                        container.contentWindow.scrollTo (x, y);
                    }
                    resolver (contenedor);
                }
            }, 50);
        };

        documentClone.open ();
        documentClone.write ("<! DOCTYPE html> <html> </html>");
        // Chrome desplaza el documento principal por alguna raz�n despu�s de la escritura en la ventana clonada ???
        restoreOwnerScroll (ownerDocument, x, y);
        documentClone.replaceChild (options.javascriptEnabled === true? documentClone.adoptNode (documentElement): removeScriptNodes (documentClone.adoptNode (documentElement)), documentClone.documentElement);
        documentClone.close ();
    });
}

function cloneNodeValues ??(document, clone, nodeName) {
    var originalNodes = document.getElementsByTagName (nodeName);
    var clonedNodes = clone.getElementsByTagName (nodeName);
    var count = originalNodes.length;
    para (var i = 0; i <count; i ++) {
        clonedNodes [i] .value = originalNodes [i] .value;
    }
}

funci�n restoreOwnerScroll (ownerDocument, x, y) {
    if (ownerDocument.defaultView && (x! == ownerDocument.defaultView.pageXOffset || y! == ownerDocument.defaultView.pageYOffset)) {
        ownerDocument.defaultView.scrollTo (x, y);
    }
}

funci�n loadUrlDocument (src, proxy, documento, ancho, alto, opciones) {
    devuelve Proxy nuevo (src, proxy, window.document) .then (documentFromHTML (src)). then (function (doc) {
        devuelva createWindowClone (doc, document, width, height, options, 0, 0);
    });
}

funci�n documentFromHTML (src) {
    funci�n de retorno (html) {
        var parser = new DOMParser (), doc;
        tratar {
            doc = parser.parseFromString (html, "text / html");
        } atrapar (e) {
            log ("DOMParser no es compatible, retrocediendo a createHTMLDocument");
            doc = document.implementation.createHTMLDocument ("");
            tratar {
                doc.open ();
                doc.write (html);
                doc.close ();
            } atrapar (ee) {
                log ("createHTMLDocument write no se admite, recurriendo a document.body.innerHTML");
                doc.body.innerHTML = html; // ie9 no admite la escritura en documentElement
            }
        }

        var b = doc.querySelector ("base");
        if (! b ||! b.href.host) {
            var base = doc.createElement ("base");
            base.href = src;
            doc.head.insertBefore (base, doc.head.firstChild);
        }

        volver doc;
    };
}


funci�n labelCanvasElements (ownerDocument) {
    [] .slice.call (ownerDocument.querySelectorAll ("canvas"), 0) .forEach (function (canvas) {
        canvas.setAttribute (html2canvasCanvasCloneAttribute, "canvas-" + html2canvasCanvasCloneIndex ++);
    });
}

function cloneCanvasContents (ownerDocument, documentClone) {
    [] .slice.call (ownerDocument.querySelectorAll ("[" + html2canvasCanvasCloneAttribute + "]"), 0) .forEach (function (canvas) {
        tratar {
            var clonedCanvas = documentClone.querySelector ('[' + html2canvasCanvasCloneAttribute + '= "' + canvas.getAttribute (html2canvasCanvasCloneAttribute) + '"]');
            if (clonedCanvas) {
                clonedCanvas.width = canvas.width;
                clonedCanvas.height = canvas.height;
                clonedCanvas.getContext ("2d"). putImageData (canvas.getContext ("2d"). getImageData (0, 0, canvas.width, canvas.height), 0, 0);
            }
        } atrapar (e) {
            log ("No se puede copiar el contenido del lienzo desde", lienzo, e);
        }
        canvas.removeAttribute (html2canvasCanvasCloneAttribute);
    });
}

funci�n removeScriptNodes (padre) {
    [] .slice.call (parent.childNodes, 0) .filter (isElementNode) .forEach (function (node) {
        if (node.tagName === "SCRIPT") {
            parent.removeChild (node);
        } else {
            removeScriptNodes (nodo);
        }
    });
    padre devuelto
}

funci�n isElementNode (nodo) {
    return node.nodeType === Node.ELEMENT_NODE;
}

function absoluteUrl (url) {
    var link = document.createElement ("a");
    link.href = url;
    link.href = link.href;
    enlace de retorno;
}

// http://dev.w3.org/csswg/css-color/

funci�n Color (valor) {
    esto.r = 0;
    esto.g = 0;
    esto.b = 0;
    this.a = null;
    var resultado = this.fromArray (valor) ||
        this.namedColor (valor) ||
        this.rgb (valor) ||
        this.rgba (valor) ||
        this.hex6 (valor) ||
        this.hex3 (valor);
}

Color.prototype.darken = funci�n (cantidad) {
    var a = 1 - cantidad;
    volver nuevo color ([
        Math.round (this.r * a),
        Math.round (this.g * a),
        Math.round (this.b * a),
        esto un
    ]);
};

Color.prototype.isTransparent = function () {
    devuelve this.a === 0;
};

Color.prototype.isBlack = function () {
    devuelva this.r === 0 && this.g === 0 && this.b === 0;
};

Color.prototype.fromArray = function (array) {
    if (Array.isArray (array)) {
        this.r = Math.min (array [0], 255);
        this.g = Math.min (array [1], 255);
        this.b = Math.min (array [2], 255);
        if (array.length> 3) {
            this.a = array [3];
        }
    }

    return (Array.isArray (array));
};

var _hex3 = / ^ # ([a-f0-9] {3}) $ / i;

Color.prototype.hex3 = function (value) {
    var match = null;
    if ((match = value.match (_hex3))! == null) {
        this.r = parseInt (match [1] [0] + match [1] [0], 16);
        this.g = parseInt (match [1] [1] + match [1] [1], 16);
        this.b = parseInt (match [1] [2] + match [1] [2], 16);
    }
    partido de vuelta! == nulo;
};

var _hex6 = / ^ # ([a-f0-9] {6}) $ / i;

Color.prototype.hex6 = function (value) {
    var match = null;
    if ((match = value.match (_hex6))! == null) {
        this.r = parseInt (match [1] .substring (0, 2), 16);
        this.g = parseInt (match [1] .substring (2, 4), 16);
        this.b = parseInt (match [1] .substring (4, 6), 16);
    }
    partido de vuelta! == nulo;
};


var _rgb = / ^ rgb \ ((\ d {1,3}) *, * (\ d {1,3}) *, * (\ d {1,3}) \) $ /;

Color.prototype.rgb = function (value) {
    var match = null;
    if ((match = value.match (_rgb))! == null) {
        this.r = Number (match [1]);
        this.g = N�mero (coincidencia [2]);
        this.b = N�mero (coincidencia [3]);
    }
    partido de vuelta! == nulo;
};

var _rgba = / ^ rgba \ ((\ d {1,3}) *, * (\ d {1,3}) *, * (\ d {1,3}) *, * (\ d + \.? \ d *) \) $ /;

Color.prototype.rgba = function (value) {
    var match = null;
    if ((match = value.match (_rgba))! == null) {
        this.r = Number (match [1]);
        this.g = N�mero (coincidencia [2]);
        this.b = N�mero (coincidencia [3]);
        this.a = N�mero (coincidencia [4]);
    }
    partido de vuelta! == nulo;
};

Color.prototype.toString = function () {
    devuelve this.a! == null && this.a! == 1?
    "rgba (" + [this.r, this.g, this.b, this.a] .join (",") + ")":
    "rgb (" + [this.r, this.g, this.b] .join (",") + ")";
};

Color.prototype.namedColor = function (value) {
    var color = colors [value.toLowerCase ()];
    si (color) {
        this.r = color [0];
        esto.g = color [1];
        this.b = color [2];
    } else if (value.toLowerCase () === "transparent") {
        this.r = this.g = this.b = this.a = 0;
        devuelve verdadero
    }

    volver !! color;
};

Color.prototype.isColor = true;

// JSON.stringify ([]. Slice.call ($$ ('. Named-color-table tr'), 1) .map (function (row) {return [row.childNodes [3] .textContent, row. childNodes [5] .textContent.trim (). split (","). map (N�mero)]}). reduce (funci�n (datos, fila) {datos [fila [0]] = fila [1]; datos de retorno }, {}))
var colors = {
    "aliceblue": [240, 248, 255],
    "antiquewhite": [250, 235, 215],
    "aqua": [0, 255, 255],
    "aguamarina": [127, 255, 212],
    "azure": [240, 255, 255],
    "beige": [245, 245, 220],
    "bisque": [255, 228, 196],
    "negro": [0, 0, 0],
    "blanchedalmond": [255, 235, 205],
    "azul": [0, 0, 255],
    "blueviolet": [138, 43, 226],
    "marr�n": [165, 42, 42],
    "Burlywood": [222, 184, 135],
    "cadetblue": [95, 158, 160],
    "chartreuse": [127, 255, 0],
    "chocolate": [210, 105, 30],
    "coral": [255, 127, 80],
    "cornflowerblue": [100, 149, 237],
    "cornisa": [255, 248, 220],
    "carmes�": [220, 20, 60],
    "cian": [0, 255, 255],
    "darkblue": [0, 0, 139],
    "darkcyan": [0, 139, 139],
    "darkgoldenrod": [184, 134, 11],
    "darkgray": [169, 169, 169],
    "darkgreen": [0, 100, 0],
    "darkgrey": [169, 169, 169],
    "darkkhaki": [189, 183, 107],
    "darkmagenta": [139, 0, 139],
    "darkolivegreen": [85, 107, 47],
    "darkorange": [255, 140, 0],
    "darkorchid": [153, 50, 204],
    "darkred": [139, 0, 0],
    "darksalmon": [233, 150, 122],
    "darkseagreen": [143, 188, 143],
    "darkslateblue": [72, 61, 139],
    "darkslategray": [47, 79, 79],
    "darkslategrey": [47, 79, 79],
    "darkturquoise": [0, 206, 209],
    "darkviolet": [148, 0, 211],
    "deeppink": [255, 20, 147],
    "deepskyblue": [0, 191, 255],
    "dimgray": [105, 105, 105],
    "dimgrey": [105, 105, 105],
    "dodgerblue": [30, 144, 255],
    "ladrillo de fuego": [178, 34, 34],
    "floralwhite": [255, 250, 240],
    "forestgreen": [34, 139, 34],
    "fucsia": [255, 0, 255],
    "gainboro": [220, 220, 220],
    "ghostwhite": [248, 248, 255],
    "oro": [255, 215, 0],
    "vara de oro": [218, 165, 32],
    "gris": [128, 128, 128],
    "verde": [0, 128, 0],
    "greenyellow": [173, 255, 47],
    "gris": [128, 128, 128],
    "honeydew": [240, 255, 240],
    "hotpink": [255, 105, 180],
    "indianred": [205, 92, 92],
    "�ndigo": [75, 0, 130],
    "marfil": [255, 255, 240],
    "caqui": [240, 230, 140],
    "lavanda": [230, 230, 250],
    "lavenderblush": [255, 240, 245],
    "lawngreen": [124, 252, 0],
    "lemonchiffon": [255, 250, 205],
    "lightblue": [173, 216, 230],
    "lightcoral": [240, 128, 128],
    "lightcyan": [224, 255, 255],
    "lightgoldenrodyellow": [250, 250, 210],
    "lightgray": [211, 211, 211],
    "verde claro": [144, 238, 144],
    "lightgrey": [211, 211, 211],
    "lightpink": [255, 182, 193],
    "luminiscencia": [255, 160, 122],
    "lightseagreen": [32, 178, 170],
    "lightskyblue": [135, 206, 250],
    "lightslategray": [119, 136, 153],
    "lightslategrey": [119, 136, 153],
    "lightsteelblue": [176, 196, 222],
    "lightyellow": [255, 255, 224],
    "lima": [0, 255, 0],
    "limegreen": [50, 205, 50],
    "lino": [250, 240, 230],
    "magenta": [255, 0, 255],
    "granate": [128, 0, 0],
    "mediumaquamarine": [102, 205, 170],
    "mediumblue": [0, 0, 205],
    "mediumorchid": [186, 85, 211],
    "mediumpurple": [147, 112, 219],
    "mediumseagreen": [60, 179, 113],
    "mediumslateblue": [123, 104, 238],
    "mediumspringgreen": [0, 250, 154],
    "mediumturquoise": [72, 209, 204],
    "mediumvioletred": [199, 21, 133],
    "midnightblue": [25, 25, 112],
    "mintcream": [245, 255, 250],
    "mistyrose": [255, 228, 225],
    "mocas�n": [255, 228, 181],
    "navajowhite": [255, 222, 173],
    "Marina": [0, 0, 128],
    "oldlace": [253, 245, 230],
    "oliva": [128, 128, 0],
    "olivedrab": [107, 142, 35],
    "naranja": [255, 165, 0],
    "orangered": [255, 69, 0],
    "orqu�dea": [218, 112, 214],
    "palegoldenrod": [238, 232, 170],
    "palegreen": [152, 251, 152],
    "paleturquoise": [175, 238, 238],
    "palevioletred": [219, 112, 147],
    "papayawhip": [255, 239, 213],
    "peachpuff": [255, 218, 185],
    "Per�": [205, 133, 63],
    "rosa": [255, 192, 203],
    "ciruela": [221, 160, 221],
    "powderblue": [176, 224, 230],
    "p�rpura": [128, 0, 128],
    "rebeccapurple": [102, 51, 153],
    "rojo": [255, 0, 0],
    "Rosybrown": [188, 143, 143],
    "royalblue": [65, 105, 225],
    "saddlebrown": [139, 69, 19],
    "salm�n": [250, 128, 114],
    "Sandybrown": [244, 164, 96],
    "seagreen": [46, 139, 87],
    "concha marina": [255, 245, 238],
    "sienna": [160, 82, 45],
    "plata": [192, 192, 192],
    "skyblue": [135, 206, 235],
    "slateblue": [106, 90, 205],
    "Slategray": [112, 128, 144],
    "slategrey": [112, 128, 144],
    "nieve": [255, 250, 250],
    "springgreen": [0, 255, 127],
    "steelblue": [70, 130, 180],
    "tan": [210, 180, 140],
    "verde azulado": [0, 128, 128],
    "cardo": [216, 191, 216],
    "tomate": [255, 99, 71],
    "turquesa": [64, 224, 208],
    "violeta": [238, 130, 238],
    "trigo": [245, 222, 179],
    "blanco": [255, 255, 255],
    "humo blanco": [245, 245, 245],
    "amarillo": [255, 255, 0],
    "yellowgreen": [154, 205, 50]
};


funci�n DummyImageContainer (src) {
    this.src = src;
    log ("DummyImageContainer for", src);
    if (! this.promise ||! this.image) {
        log ("Initiating DummyImageContainer");
        DummyImageContainer.prototype.image = new Image ();
        var image = this.image;
        DummyImageContainer.prototype.promise = new Promesa (funci�n (resolver, rechazar) {
            image.onload = resolver;
            image.onerror = rechazar;
            image.src = smallImage ();
            if (image.complete === true) {
                resolver (imagen);
            }
        });
    }
}

Funci�n de fuente (familia, tama�o) {
    var container = document.createElement ('div'),
        img = document.createElement ('img'),
        span = document.createElement ('span'),
        sampleText = 'Texto oculto',
        base,
        medio;

    container.style.visibility = "oculto";
    container.style.fontFamily = family;
    container.style.fontSize = tama�o;
    container.style.margin = 0;
    container.style.padding = 0;

    document.body.appendChild (contenedor);

    img.src = smallImage ();
    img.width = 1;
    img.height = 1;

    img.style.margin = 0;
    img.style.padding = 0;
    img.style.verticalAlign = "baseline";

    span.style.fontFamily = family;
    span.style.fontSize = tama�o;
    span.style.margin = 0;
    span.style.padding = 0;

    span.appendChild (document.createTextNode (sampleText));
    container.appendChild (span);
    container.appendChild (img);
    baseline = (img.offsetTop - span.offsetTop) + 1;

    container.removeChild (span);
    container.appendChild (document.createTextNode (sampleText));

    container.style.lineHeight = "normal";
    img.style.verticalAlign = "super";

    middle = (img.offsetTop-container.offsetTop) + 1;

    document.body.removeChild (contenedor);

    this.baseline = baseline;
    this.lineWidth = 1;
    this.middle = middle;
}

funci�n FontMetrics () {
    esto.data = {};
}

FontMetrics.prototype.getMetrics = function (family, size) {
    if (this.data [family + "-" + size] === undefined) {
        this.data [familia + "-" + tama�o] = nueva Fuente (familia, tama�o);
    }
    devuelve this.data [family + "-" + size];
};

Funci�n FrameContainer (contenedor, sameOrigin, opciones) {
    this.image = null;
    this.src = contenedor;
    var self = esto;
    l�mites var = getBounds (contenedor);
    this.promise = (! sameOrigin? this.proxyLoad (options.proxy ,mites, opciones): nueva Promesa (funci�n (resolver) {
        if (container.contentWindow.document.URL === "about: blank" || container.contentWindow.document.documentElement == null) {
            container.contentWindow.onload = container.onload = function () {
                resolver (contenedor);
            };
        } else {
            resolver (contenedor);
        }
    })). Luego (funci�n (contenedor) {
        return html2canvas (container.contentWindow.document.documentElement, {type: 'view', width: container.width, height: container.height, proxy: options.proxy, javascriptEnabled: options.javascriptEnabled, removeContainer: options.removeContainer, allowTaint: options.allowTaint, imageTimeout: options.imageTimeout / 2});
    }). luego (funci�n (lienzo) {
        return self.image = canvas;
    });
}

FrameContainer.prototype.proxyLoad = function (proxy, l�mites, opciones) {
    contenedor var = this.src;
    devolver loadUrlDocument (container.src, proxy, container.ownerDocument, lines.width, limits.height, opciones);
};

funci�n GradientContainer (imageData) {
    this.src = imageData.value;
    this.colorStops = [];
    this.type = null;
    esto.x0 = 0.5;
    this.y0 = 0.5;
    esto.x1 = 0.5;
    this.y1 = 0.5;
    this.promise = Promise.resolve (true);
}

GradientContainer.prototype.TYPES = {
    LINEAR: 1,
    RADIAL: 2
};

funci�n ImageContainer (src, cors) {
    this.src = src;
    this.image = new Image ();
    var self = esto;
    this.tainted = null;
    this.promise = new Promise (funci�n (resolver, rechazar) {
        self.image.onload = resolver;
        self.image.onerror = rechazar;
        if (cors) {
            self.image.crossOrigin = "an�nimo";
        }
        self.image.src = src;
        if (self.image.complete === true) {
            resolver (self.image);
        }
    });
}

funci�n ImageLoader (opciones, soporte) {
    this.link = null;
    this.options = opciones;
    this.support = support;
    this.origin = this.getOrigin (window.location.href);
}

ImageLoader.prototype.findImages = function (nodos) {
    var imagenes = [];
    nodes.reduce (funci�n (imageNodes, contenedor) {
        switch (container.node.nodeName) {
        caso "IMG":
            devuelve imageNodes.concat ([{
                Args: [container.node.src],
                m�todo: "url"
            }]);
        caso "svg":
        caso "IFRAME":
            devuelve imageNodes.concat ([{
                Args: [container.node],
                m�todo: container.node.nodeName
            }]);
        }
        devuelve imageNodes;
    }, []). forEach (this.addImage (images, this.loadImage), this);
    im�genes de retorno;
};

ImageLoader.prototype.findBackgroundImage = function (images, container) {
    container.parseBackgroundImages (). filter (this.hasImageBackground) .forEach (this.addImage (images, this.loadImage), this);
    im�genes de retorno;
};

ImageLoader.prototype.addImage = function (images, callback) {
    funci�n de retorno (newImage) {
        newImage.args.forEach (funci�n (imagen) {
            if (! this.imageExists (im�genes, imagen)) {
                images.splice (0, 0, callback.call (this, newImage));
                log ('Added image #' + (images.length), typeof (image) === "string"? image.substring (0, 100): image);
            }
        }, esta);
    };
};

ImageLoader.prototype.hasImageBackground = function (imageData) {
    devuelve imageData.method! == "none";
};

ImageLoader.prototype.loadImage = function (imageData) {
    if (imageData.method === "url") {
        var src = imageData.args [0];
        if (this.isSVG (src) &&! this.support.svg &&! this.options.allowTaint) {
            devolver nuevo SVGContainer (src);
        } else if (src.match (/ data: image \ /.*; base64, / i)) {
            devolver el nuevo ImageContainer (src.replace (/ url \ (['"] {0,} | ['"] {0,} \) $ / ig, ''), false);
        } else if (this.isSameOrigin (src) || ??this.options.allowTaint === true || this.isSVG (src)) {
            devolver nuevo ImageContainer (src, false);
        } else if (this.support.cors &&! this.options.allowTaint && this.options.useCORS) {
            devuelve nuevo ImageContainer (src, true);
        } else if (this.options.proxy) {
            devuelve el nuevo ProxyImageContainer (src, this.options.proxy);
        } else {
            devolver nuevo DummyImageContainer (src);
        }
    } else if (imageData.method === "gradiente lineal") {
        devuelve el nuevo LinearGradientContainer (imageData);
    } else if (imageData.method === "gradiente") {
        devuelve el nuevo WebkitGradientContainer (imageData);
    } else if (imageData.method === "svg") {
        devuelve el nuevo SVGNodeContainer (imageData.args [0], this.support.svg);
    } else if (imageData.method === "IFRAME") {
        devuelve el nuevo FrameContainer (imageData.args [0], this.isSameOrigin (imageData.args [0] .src), this.options);
    } else {
        devuelve el nuevo DummyImageContainer (imageData);
    }
};

ImageLoader.prototype.isSVG = function (src) {
    return src.substring (src.length - 3) .toLowerCase () === "svg" || SVGContainer.prototype.isInline (src);
};

ImageLoader.prototype.imageExists = function (images, src) {
    devuelve images.some (funci�n (imagen) {
        devuelve image.src === src;
    });
};

ImageLoader.prototype.isSameOrigin = function (url) {
    return (this.getOrigin (url) === this.origin);
};

ImageLoader.prototype.getOrigin = function (url) {
    var link = this.link || (this.link = document.createElement ("a"));
    link.href = url;
    link.href = link.href; // IE9, LOL! - http://jsfiddle.net/niklasvh/2e48b/
    devolver link.protocol + link.hostname + link.port;
};

ImageLoader.prototype.getPromise = function (container) {
    devuelve this.timeout (container, this.options.imageTimeout) ['catch'] (function () {
        var dummy = new DummyImageContainer (container.src);
        devolver dummy.promise.then (function (image) {
            container.image = imagen;
        });
    });
};

ImageLoader.prototype.get = function (src) {
    var encontrado = nulo;
    devuelve this.images.some (function (img) {
        return (found = img) .src === src;
    })? encontrado: nulo;
};

ImageLoader.prototype.fetch = function (nodos) {
    this.images = nodes.reduce (bind (this.findBackgroundImage, this), this.findImages (nodos));
    this.images.forEach (funci�n (imagen, �ndice) {
        image.promise.then (function () {
            log ("Imagen cargada correctamente #" + (�ndice + 1), imagen);
        }, funci�n (e) {
            log ("Error al cargar la imagen #" + (�ndice + 1), imagen, e);
        });
    });
    this.ready = Promise.all (this.images.map (this.getPromise, this));
    log ("Im�genes de b�squeda terminadas");
    devuelve esto
};

ImageLoader.prototype.timeout = function (container, timeout) {
    temporizador var
    var promise = Promise.race ([container.promise, new Promise (funci�n (res, rechazar) {
        timer = setTimeout (function () {
            registro ("Imagen de tiempo de espera agotada", contenedor);
            rechazar (contenedor);
        }, se acab� el tiempo);
    })]). Luego (funci�n (contenedor) {
        clearTimeout (temporizador);
        contenedor de retorno
    });
    promesa ['catch'] (function () {
        clearTimeout (temporizador);
    });
    promesa de devoluci�n
};

funci�n LinearGradientContainer (imageData) {
    GradientContainer.apply (esto, argumentos);
    this.type = this.TYPES.LINEAR;

    var hasDirection = imageData.args [0] .match (this.stepRegExp) === null;

    if (hasDirection) {
        imageData.args [0] .split ("") .reverse (). forEach (function (position) {
            interruptor (posici�n) {
            caso "izquierda":
                esto.x0 = 0;
                this.x1 = 1;
                descanso;
            caso "top":
                this.y0 = 0;
                this.y1 = 1;
                descanso;
            caso "correcto":
                esto.x0 = 1;
                this.x1 = 0;
                descanso;
            caso "abajo":
                this.y0 = 1;
                this.y1 = 0;
                descanso;
            caso "a":
                var y0 = this.y0;
                var x0 = este.x0;
                this.y0 = this.y1;
                this.x0 = this.x1;
                esto.x1 = x0;
                this.y1 = y0;
                descanso;
            }
        }, esta);
    } else {
        this.y0 = 0;
        this.y1 = 1;
    }

    this.colorStops = imageData.args.slice (hasDirection? 1: 0) .map (function (colorStop) {
        var colorStopMatch = colorStop.match (this.stepRegExp);
        regreso {
            color: nuevo Color (colorStopMatch [1]),
            detener: colorStopMatch [3] === "%"? colorStopMatch [2] / 100: null
        };
    }, esta);

    if (this.colorStops [0] .stop === null) {
        this.colorStops [0] .stop = 0;
    }

    if (this.colorStops [this.colorStops.length - 1] .stop === null) {
        this.colorStops [this.colorStops.length - 1] .stop = 1;
    }

    this.colorStops.forEach (funci�n (colorStop, �ndice) {
        if (colorStop.stop === null) {
            this.colorStops.slice (index) .some (function (find, count) {
                if (find.stop! == null) {
                    colorStop.stop = ((find.stop - this.colorStops [index - 1] .stop) / (count + 1)) + this.colorStops [index - 1] .stop;
                    devuelve verdadero
                } else {
                    falso retorno;
                }
            }, esta);
        }
    }, esta);
}

LinearGradientContainer.prototype = Object.create (GradientContainer.prototype);

LinearGradientContainer.prototype.stepRegExp = / ((?: Rgb | rgba) \ (\ d {1,3}, \ s \ d {1,3}, \ s \ d {1,3} (?:, \ S [0-9 \.] +)? \)) \ S * (\ d {1,3})? (% | Px)? /;

registro de funciones () {
    if (window.html2canvas.logging && window.console && window.console.log) {
        Function.prototype.bind.call (window.console.log, (window.console)). Apply (window.console, [(Date.now () - window.html2canvas.start) + "ms", "html2canvas:" ] .concat ([]. slice.call (argumentos, 0)));
    }
}

funci�n NodeContainer (nodo, padre) {
    este.nodo = nodo;
    this.parent = padre;
    this.stack = null;
    this.bounds = null;
    this.borders = null;
    este.clip = [];
    this.backgroundClip = [];
    this.offsetBounds = null;
    this.visible = null;
    this.computedStyles = null;
    esto.colores = {};
    this.styles = {};
    this.backgroundImages = null;
    this.transformData = null;
    this.transformMatrix = null;
    this.isPseudoElement = false;
    this.opacity = null;
}

NodeContainer.prototype.cloneTo = function (stack) {
    stack.visible = this.visible;
    stack.borders = this.borders;
    stack.bounds = this.bounds;
    stack.clip = this.clip;
    stack.backgroundClip = this.backgroundClip;
    stack.computedStyles = this.computedStyles;
    stack.styles = this.styles;
    stack.backgroundImages = this.backgroundImages;
    stack.opacity = this.opacity;
};

NodeContainer.prototype.getOpacity = function () {
    devuelve this.opacity === null? (this.opacity = this.cssFloat ('opacity')): this.opacity;
};

NodeContainer.prototype.assignStack = function (stack) {
    this.stack = pila;
    stack.children.push (esto);
};

NodeContainer.prototype.isElementVisible = function () {
    devuelve this.node.nodeType === Node.TEXT_NODE? este.parente.visible:
        this.css ('display')! == "none" &&
        this.css ('visibilidad')! == "oculto" &&
        ! this.node.hasAttribute ("data-html2canvas-ignore") &&
        (this.node.nodeName! == "INPUT" || this.node.getAttribute ("type")! == "hidden")
    );
};

NodeContainer.prototype.css = funci�n (atributo) {
    if (! this.computedStyles) {
        this.computedStyles = this.isPseudoElement? this.parent.computedStyle (this.before? ": before": ": after"): this.computedStyle (null);
    }

    devuelve this.styles [atributo] || (this.styles [atributo] = this.computedStyles [atributo]);
};

NodeContainer.prototype.prefixedCss = function (atributo) {
    prefijos var = ["webkit", "moz", "ms", "o"];
    valor de var = this.css (atributo);
    si (valor === indefinido) {
        prefixes.some (funci�n (prefijo) {
            value = this.css (prefix + attribute.substr (0, 1) .toUpperCase () + attribute.substr (1));
            valor de retorno! == indefinido;
        }, esta);
    }
    valor de retorno === indefinido? valor nulo;
};

NodeContainer.prototype.computedStyle = function (type) {
    devuelve this.node.ownerDocument.defaultView.getComputedStyle (this.node, type);
};

NodeContainer.prototype.cssInt = function (atributo) {
    var value = parseInt (this.css (atributo), 10);
    devuelve (isNaN (valor))? 0: valor; // los bordes en el antiguo IE est�n lanzando 'medium' para demo.html
};

NodeContainer.prototype.color = function (atributo) {
    devuelve this.colors [atributo] || (this.colors [atributo] = nuevo Color (this.css (atributo)));
};

NodeContainer.prototype.cssFloat = function (atributo) {
    var value = parseFloat (this.css (atributo));
    devuelve (isNaN (valor))? 0: valor;
};

NodeContainer.prototype.fontWeight = function () {
    var weight = this.css ("fontWeight");
    interruptor (parseInt (peso, 10)) {
    caso 401:
        peso = "negrita";
        descanso;
    caso 400:
        peso = "normal";
        descanso;
    }
    peso de vuelta
};

NodeContainer.prototype.parseClip = function () {
    var matches = this.css ('clip'). match (this.CLIP);
    si (coincide) {
        regreso {
            arriba: parseInt (coincidencias [1], 10),
            derecha: parseInt (coincidencias [2], 10),
            abajo: parseInt (coincidencias [3], 10),
            izquierda: parseInt (coincidencias [4], 10)
        };
    }
    retorno nulo
};

NodeContainer.prototype.parseBackgroundImages = function () {
    devuelve this.backgroundImages || (this.backgroundImages = parseBackgrounds (this.css ("backgroundImage")));
};

NodeContainer.prototype.cssList = function (propiedad, �ndice) {
    var value = (this.css (propiedad) || '') .split (',');
    valor = valor [�ndice || 0] || valor [0] || 'auto';
    value = value.trim (). split ('');
    if (value.length === 1) {
        valor = [valor [0], valor [0]];
    }
    valor de retorno;
};

NodeContainer.prototype.parseBackgroundSize = funci�n (l�mites, imagen, �ndice) {
    var size = this.cssList ("backgroundSize", �ndice);
    ancho var, alto;

    if (isPercentage (size [0])) {
        width = lines.width * parseFloat (tama�o [0]) / 100;
    } else if (/contain|cover/.test(s Mapa de la imagen0])) {
        var targetRatio = lines.width / lines.height, currentRatio = image.width / image.height;
        return (targetRatio <currentRatio ^ tama�o [0] === 'contener')? {width: lines.height * currentRatio, height: limits.height}: {width: fields.width, height: bound.width / currentRatio};
    } else {
        ancho = parseInt (tama�o [0], 10);
    }

    if (tama�o [0] === 'auto' && tama�o [1] === 'auto') {
        altura = imagen. altura;
    } else if (tama�o [1] === 'auto') {
        altura = ancho / image.width * image.height;
    } else if (isPercentage (size [1])) {
        height = lines.height * parseFloat (tama�o [1]) / 100;
    } else {
        height = parseInt (tama�o [1], 10);
    }

    if (tama�o [0] === 'auto') {
        width = height / image.height * image.width;
    }

    return {ancho: ancho, alto: alto};
};

NodeContainer.prototype.parseBackgroundPosition = funci�n (l�mites, imagen, �ndice, fondo de tama�o) {
    var position = this.cssList ('backgroundPosition', index);
    var izquierda, arriba;

    if (isPercentage (position [0])) {
        left = (lines.width - (backgroundSize || image) .width) * (parseFloat (position [0]) / 100);
    } else {
        izquierda = parseInt (posici�n [0], 10);
    }

    if (posici�n [1] === 'auto') {
        arriba = izquierda / image.width * image.height;
    } else if (esPorcentaje (posici�n [1])) {
        top = (lines.height - (backgroundSize || image) .height) * parseFloat (position [1]) / 100;
    } else {
        arriba = parseInt (posici�n [1], 10);
    }

    if (posici�n [0] === 'auto') {
        left = top / image.height * image.width;
    }

    return {left: left, top: top};
};

NodeContainer.prototype.parseBackgroundRepeat = function (index) {
    devuelve this.cssList ("backgroundRepeat", index) [0];
};

NodeContainer.prototype.parseTextShadows = function () {
    var textShadow = this.css ("textShadow");
    resultados de var = [];

    if (textShadow && textShadow! == 'none') {
        var shadows = textShadow.match (this.TEXT_SHADOW_PROPERTY);
        para (var i = 0; shadows && (i <shadows.length); i ++) {
            var s = shadows [i] .match (this.TEXT_SHADOW_VALUES);
            resultados.push ({
                color: nuevo Color (s [0]),
                offsetX: s [1]? parseFloat (s [1] .replace ('px', '')): 0,
                offsetY: s [2]? parseFloat (s [2] .replace ('px', '')): 0,
                desenfoque: s [3]? s [3] .replace ('px', ''): 0
            });
        }
    }
    resultados de retorno
};

NodeContainer.prototype.parseTransform = function () {
    if (! this.transformData) {
        if (this.hasTransform ()) {
            var offset = this.parseBounds ();
            var origin = this.prefixedCss ("transformOrigin"). split ("") .map (removePx) .map (asFloat);
            origen [0] + = offset.left;
            origen [1] + = offset.top;
            this.transformData = {
                origen:
                matriz: this.parseTransformMatrix ()
            };
        } else {
            this.transformData = {
                origen: [0, 0],
                matriz: [1, 0, 0, 1, 0, 0]
            };
        }
    }
    devuelve this.transformData;
};

NodeContainer.prototype.parseTransformMatrix = function () {
    if (! this.transformMatrix) {
        var transform = this.prefixedCss ("transform");
        var matriz = transformar? parseMatrix (transform.match (this.MATRIX_PROPERTY)): null;
        this.transformMatrix = matrix? matriz: [1, 0, 0, 1, 0, 0];
    }
    devuelve this.transformMatrix;
};

NodeContainer.prototype.parseBounds = function () {
    devuelve esto.bounds || (this.bounds = this.hasTransform ()? offsetBounds (this.node): getBounds (this.node));
};

NodeContainer.prototype.hasTransform = function () {
    devuelve this.parseTransformMatrix (). join (",")! == "1,0,0,1,0,0" || (this.parent && this.parent.hasTransform ());
};

NodeContainer.prototype.getValue = function () {
    valor var = este.nodo.valor || "";
    if (this.node.tagName === "SELECT") {
        value = selectionValue (this.node);
    } else if (this.node.type === "contrase�a") {
        value = Array (value.length + 1) .join ('\ u2022'); // jshint ignore: line
    }
    devuelve value.length === 0? (this.node.placeholder || ""): valor;
};

NodeContainer.prototype.MATRIX_PROPERTY = /(matrix)\((.+)\)/;
NodeContainer.prototype.TEXT_SHADOW_PROPERTY = / ((rgba | rgb) \ ([^ \)] + \) (\ s -? \ D + px) {0,}) / g;
NodeContainer.prototype.TEXT_SHADOW_VALUES = /(-?\d+px)|(#.+)|(rgb\(.+\))|(rgba\(.+\))/g;
NodeContainer.prototype.CLIP = / ^ rect \ ((\ d +) px ,? (\ d +) px ,? (\ d +) px ,? (\ d +) px \) $ /;

funci�n selectionValue (nodo) {
    var option = node.options [node.selectedIndex || 0];
    opci�n de retorno? (option.text || ""): "";
}

funci�n parseMatrix (match) {
    if (match && match [1] === "matrix") {
        devolver coincidencia [2] .split (","). map (funci�n (es) {
            devuelve parseFloat (s.trim ());
        });
    }
}

funci�n isPercentage (value) {
    devuelve value.toString (). indexOf ("%")! == -1;
}

funci�n parseBackgrounds (backgroundImage) {
    var whitespace = '\ r \ n \ t',
        m�todo, definici�n, prefijo, prefijo_i, bloque, resultados = [],
        mode = 0, numParen = 0, quote, args;
    var appendResult = function () {
        if (m�todo) {
            if (definition.substr (0, 1) === '"') {
                definition = definition.substr (1, definition.length - 2);
            }
            si (definici�n) {
                args.push (definici�n);
            }
            if (method.substr (0, 1) === '-' && (prefix_i = method.indexOf ('-', 1) + 1)> 0) {
                prefijo = method.substr (0, prefijo_i);
                metodo = metodo.substr (prefijo_i);
            }
            resultados.push ({
                prefijo: prefijo,
                m�todo: method.toLowerCase (),
                valor: bloque,
                args: args
                imagen: nula
            });
        }
        args = [];
        m�todo = prefijo = definici�n = bloque = '';
    };
    args = [];
    m�todo = prefijo = definici�n = bloque = '';
    backgroundImage.split (""). forEach (function (c) {
        if (mode === 0 && whitespace.indexOf (c)> -1) {
            regreso;
        }
        interruptor (c) {
        caso '"':
            si (! cita) {
                quote = c;
            } else if (quote === c) {
                quote = null;
            }
            descanso;
        caso '(':
            si (cita) {
                descanso;
            } else if (modo === 0) {
                modo = 1;
                bloque + = c;
                regreso;
            } else {
                numParen ++;
            }
            descanso;
        caso ')':
            si (cita) {
                descanso;
            } else if (modo === 1) {
                if (numParen === 0) {
                    modo = 0;
                    bloque + = c;
                    appendResult ();
                    regreso;
                } else {
                    numParen--;
                }
            }
            descanso;

        caso ',':
            si (cita) {
                descanso;
            } else if (modo === 0) {
                appendResult ();
                regreso;
            } else if (modo === 1) {
                if (numParen === 0 &&! method.match (/ ^ url $ / i)) {
                    args.push (definici�n);
                    definition = '';
                    bloque + = c;
                    regreso;
                }
            }
            descanso;
        }

        bloque + = c;
        si (modo === 0) {
            m�todo + = c;
        } else {
            definici�n + = c;
        }
    });

    appendResult ();
    resultados de retorno
}

funci�n removePx (str) {
    return str.replace ("px", "");
}

funci�n asFloat (str) {
    devuelve parseFloat (str);
}

funci�n getBounds (nodo) {
    if (node.getBoundingClientRect) {
        var clientRect = node.getBoundingClie ntRect ();
        var width = node.offsetWidth == null? clientRect.width: node.offsetWidth;
        regreso {
            arriba: clientRect.top,
            abajo: clientRect.bottom || (clientRect.top + clientRect.height),
            derecha: clientRect.left + width,
            izquierda: clientRect.left,
            ancho: ancho,
            height: node.offsetHeight == null? clientRect.height: node.offsetHeight
        };
    }
    regreso {};
}

funci�n offsetBounds (nodo) {
    var parent = node.offsetParent? offsetBounds (node.offsetParent): {top: 0, left: 0};

    regreso {
        arriba: node.offsetTop + parent.top,
        abajo: node.offsetTop + node.offsetHeight + parent.top,
        derecha: node.offsetLeft + parent.left + node.offsetWidth,
        izquierda: node.offsetLeft + parent.left,
        ancho: node.offsetWidth,
        altura: node.offsetHeight
    };
}

funci�n NodeParser (elemento, procesador, soporte, imageLoader, opciones) {
    log ("Starting NodeParser");
    this.renderer = renderer;
    this.options = opciones;
    this.range = null;
    this.support = support;
    this.renderQueue = [];
    this.stack = new StackingContext (true, 1, element.ownerDocument, null);
    var parent = new NodeContainer (element, null);
    if (options.background) {
        renderer.rectangle (0, 0, renderer.width, renderer.height, nuevo Color (options.background));
    }
    if (element === element.ownerDocument.documentElement) {
        // http://www.w3.org/TR/css3-background/#special-backgrounds
        var canvasBackground = new NodeContainer (parent.color ('backgroundColor'). isTransparent ()? element.ownerDocument.body: element.ownerDocument.documentElement, null);
        renderer.rectangle (0, 0, renderer.width, renderer.height, canvasBackground.color ('backgroundColor'));
    }
    parent.visibile = parent.isElementVisible ();
    this.createPseudoHideStyles (element.ownerDocument);
    this.disableAnimations (element.ownerDocument);
    this.nodes = flatten ([parent] .concat (this.getChildren (parent)). filter (function (container) {
        return container.visible = container.isElementVisible ();
    }). map (this.getPseudoElements, this));
    this.fontMetrics = new FontMetrics ();
    log ("Nodos capturados, total:", this.nodes.length);
    registro ("Calcular clips de desbordamiento");
    this.calculateOverflowClips ();
    log ("Empezar a buscar im�genes");
    this.images = imageLoader.fetch (this.nodes.filter (isElement));
    this.ready = this.images.ready.then (bind (function () {
        log ("Im�genes cargadas, an�lisis inicial");
        log ("Creando contextos de apilamiento");
        this.createStackingContexts ();
        log ("Ordenando contextos de apilamiento");
        this.sortStackingContexts (this.stack);
        this.parse (this.stack);
        log ("Hacer cola creada con" + this.renderQueue.length + "items");
        devolver nueva Promesa (vincular (funci�n (resolver) {
            if (! options.async) {
                this.renderQueue.forEach (this.paint, this);
                resolver();
            } else if (typeof (options.async) === "function") {
                options.async.call (this, this.renderQueue, resolver);
            } else if (this.renderQueue.length> 0) {
                this.renderIndex = 0;
                this.asyncRenderer (this.renderQueue, resolver);
            } else {
                resolver();
            }
        }, esta));
    }, esta));
}

NodeParser.prototype.calculateOverflowClips = function () {
    this.nodes.forEach (funci�n (contenedor) {
        if (isElement (container)) {
            if (isPseudoElement (container)) {
                container.appendToDOM ();
            }
            container.borders = this.parseBorders (container);
            var clip = (container.css ('overflow') === "hidden")? [container.borders.clip]: [];
            var cssClip = container.parseClip ();
            if (cssClip && ["absolute", "fixed"]. indexOf (container.css ('position'))! == -1) {
                clip.push ([["rect",
                        container.bounds.left + cssClip.left,
                        container.bounds.top + cssClip.top,
                        cssClip.right - cssClip.left,
                        cssClip.bottom - cssClip.top
                ]]);
            }
            container.clip = hasParentClip (container)? container.parent.clip.concat (clip): clip;
            container.backgroundClip = (container.css ('overflow')! == "hidden")? container.clip.concat ([container.borders.clip]): container.clip;
            if (isPseudoElement (container)) {
                container.cleanDOM ();
            }
        } else if (isTextNode (contenedor)) {
            container.clip = hasParentClip (container)? container.parent.clip: [];
        }
        if (! isPseudoElement (container)) {
            container.bounds = null;
        }
    }, esta);
};

funci�n hasParentClip (contenedor) {
    devolver container.parent && container.parent.clip.length;
}

NodeParser.prototype.asyncRenderer = funci�n (cola, resoluci�n, asyncTimer) {
    asyncTimer = asyncTimer || Date.now ();
    this.paint (cola [this.renderIndex ++]);
    if (queue.length === this.renderIndex) {
        resolver();
    } else if (asyncTimer + 20> Date.now ()) {
        this.asyncRenderer (cola, resoluci�n, asyncTimer);
    } else {
        setTimeout (bind (function () {
            this.asyncRenderer (cola, resoluci�n);
        }, esto), 0);
    }
};

NodeParser.prototype.createPseudoHideStyles = function (document) {
    this.createStyles (document, '.' + PseudoElementContainer.prototype.PSEUDO_HIDE_ELEMENT_CLASS_BEFORE + ': antes de {content: ""! important; display: none! important;}' +
        '.' + PseudoElementContainer.prototype.PSEUDO_HIDE_ELEMENT_CLASS_AFTER + ': after {content: ""! Important; pantalla: ninguna! importante; } ');
};

NodeParser.prototype.disableAnimations = function (document) {
    this.createStyles (documento, '* {-webkit-animation: none! important; -moz-animation: none! important; -o-animation: none! important; animaci�n: none! important;' +
        '-webkit-transici�n: ninguno! importante; -moz-transici�n: ninguna! importante; -o-transici�n: ninguna! importante; transici�n: ninguna! importante;} ');
};

NodeParser.prototype.createStyles = function (documento, estilos) {
    var hidePseudoElements = document.createElement ('style');
    hidePseudoElements.innerHTML = styles;
    document.body.appendChild (hidePseudoElements);
};

NodeParser.prototype.getPseudoElements = function (container) {
    var nodos = [[contenedor]];
    if (container.node.nodeType === Node.ELEMENT_NODE) ??{
        var before = this.getPseudoElement (container, ": before");
        var after = this.getPseudoElement (container, ": after");

        si (antes) {
            nodes.push (antes);
        }

        si (despu�s) {
            nodes.push (despues);
        }
    }
    retorno aplanado (nodos);
};

funci�n toCamelCase (str) {
    return str.replace (/ (\ - [az]) / g, function (match) {
        devuelve match.toUpperCase (). replace ('-', '');
    });
}

NodeParser.prototype.getPseudoElement = function (container, type) {
    var style = container.computedStyle (type);
    if (! style ||! style.content || style.content === "none" || style.content === "-moz-alt-content" || style.display === "none") {
        retorno nulo
    }

    var content = stripQuotes (style.content);
    var isImage = content.substr (0, 3) === 'url';
    var pseudoNode = document.createElement (isImage? 'img': 'html2canvaspseudoelement');
    var pseudoContainer = new PseudoElementContainer (pseudoNode, container, type);

    para (var i = style.length-1; i> = 0; i--) {
        propiedad var = toCamelCase (style.item (i));
        pseudoNode.style [propiedad] = estilo [propiedad];
    }

    pseudoNode.className = PseudoElementContainer.prototype.PSEUDO_HIDE_ELEMENT_CLASS_BEFORE + "" + PseudoElementContainer.prototype.PSEUDO_HIDE_ELEMENT_CLASS_AFTER;

    if (isImage) {
        pseudoNode.src = parseBackgrounds (content) [0] .args [0];
        return [pseudoContainer];
    } else {
        var text = document.createTextNode (contenido);
        pseudoNode.appendChild (texto);
        return [pseudoContainer, nuevo TextContainer (text, pseudoContainer)];
    }
};


NodeParser.prototype.getChildren = function (parentContainer) {
    return flatten ([]. filter.call (parentContainer.node.childNodes, renderableNode) .map (function (node) {
        var container = [node.nodeType === Node.TEXT_NODE? nuevo TextContainer (node, parentContainer): new NodeContainer (node, parentContainer)]. filter (nonIgnoredElement);
        return node.nodeType === Node.ELEMENT_NODE && container.length && node.tagName! == "TEXTAREA"? (container [0] .isElementVisible ()? container.concat (this.getChildren (container [0])): []): container;
    }, esta));
};

NodeParser.prototype.newStackingContext = function (container, hasOwnStacking) {
    var stack = new StackingContext (hasOwnStacking, container.getOpacity (), container.node, container.parent);
    container.cloneTo (pila);
    var parentStack = hasOwnStacking? stack.getParentStack (this): stack.parent.stack;
    parentStack.contexts.push (pila);
    container.stack = pila;
};

NodeParser.prototype.createStackingContexts = function () {
    this.nodes.forEach (funci�n (contenedor) {
        if (isElement (contenedor) && (this.isRootElement (container) || hasOpacity (container) || isPositionedForStacking (container) || this.isBodyWithTransparentRoot (container) || container.hasTransform ())) {
            this.newStackingContext (container, true);
        } else if (isElement (container) && ((isPositioned (container) && zIndex0 (container)) || isInlineBlock (container) || isFloating (container))) {
            this.newStackingContext (container, false);
        } else {
            container.assignStack (container.parent.stack);
        }
    }, esta);
};

NodeParser.prototype.isBodyWithTransparentRoot = function (container) {
    return container.node.nodeName === "BODY" && container.parent.color ('backgroundColor'). isTransparent ();
};

NodeParser.prototype.isRootElement = function (container) {
    return container.parent === null;
};

NodeParser.prototype.sortStackingContexts = function (stack) {
    stack.contexts.sort (zIndexSort (stack.contexts.slice (0)));
    stack.contexts.forEach (this.sortStackingContexts, this);
};

NodeParser.prototype.parseTextBounds = function (container) {
    Funci�n de retorno (texto, �ndice, lista de texto) {
        if (container.parent.css ("textDecoration"). substr (0, 4)! == "none" || text.trim (). length! == 0) {
            if (this.support.rangeBounds &&! container.parent.hasTransform ()) {
                var offset = textList.slice (0, index) .join (""). length;
                devuelve this.getRangeBounds (container.node, offset, text.length);
            } else if (container.node && typeof (container.node.data) === "string") {
                var replacementNode = container.node.splitText (text.length);
                l�mites de var = this.getWrapperBounds (container.node, container.parent.hasTransform ());
                container.node = replacementNode;
                l�mites de retorno;
            }
        } else if (! this.support.rangeBounds || container.parent.hasTransform ()) {
            container.node = container.node.splitText (text.length);
        }
        regreso {};
    };
};

NodeParser.prototype.getWrapperBounds = function (node, transform) {
    var wrapper = node.ownerDocument.createElement ('html2canvaswrapper');
    var parent = node.parentNode,
        backupText = node.cloneNode (true);

    wrapper.appendChild (node.cloneNode (true));
    parent.replaceChild (wrapper, node);
    l�mites var = transformar? offsetBounds (wrapper): getBounds (wrapper);
    parent.replaceChild (backupText, wrapper);
    l�mites de retorno;
};

NodeParser.prototype.getRangeBounds = funci�n (nodo, desplazamiento, longitud) {
    var range = this.range || (this.range = node.ownerDocument.createRange ());
    range.setStart (nodo, desplazamiento);
    range.setEnd (nodo, desplazamiento + longitud);
    return range.getBoundingClientRect ();
};

funci�n ClearTransform () {}

NodeParser.prototype.parse = function (stack) {
    // http://www.w3.org/TR/CSS21/visuren.html#z-index
    var negativeZindex = stack.contexts.filter (negativeZIndex); // 2. los contextos de apilamiento secundarios con niveles de pila negativos (la mayor�a de los negativos primero).
    var descendantElements = stack.children.filter (isElement);
    var descendantNonFloats = descendantElements.filter (not (isFloating));
    var nonInlineNonPositionedDescendants = descendantNonFloats.filter (not (isPositioned)). filter (not (inlineLevel)); // 3 los descendientes en el flujo, no en el nivel de l�nea, no posicionados.
    var nonPositionedFloats = descendantElements.filter (not (isPositioned)). filter (isFloating); // 4. Los flotadores no posicionados.
    var inFlow = descendantNonFloats.filter (not (isPositioned)). filter (inlineLevel); // 5. los descendientes en el flujo, en el nivel en l�nea, no posicionados, incluidas las tablas en l�nea y los bloques en l�nea.
    var stackLevel0 = stack.contexts.concat (descendantNonFloats.filter (isPositioned)). filter (zIndex0); // 6. los contextos de apilamiento secundarios con nivel de pila 0 y los descendientes posicionados con nivel de pila 0.
    var text = stack.children.filter (isTextNode) .filter (hasText);
    var positiveZindex = stack.contexts.filter (positiveZIndex); // 7. los contextos de apilamiento secundarios con niveles de pila positivos (menos positivos primero).
    negativeZindex.concat (nonInlineNonPositionedDescendants) .concat (nonPositionedFloats)
        .concat (inFlow) .concat (stackLevel0) .concat (text) .concat (positiveZindex) .forEach (function (container) {
            this.renderQueue.push (contenedor);
            if (isStackingContext (container)) {
                this.parse (contenedor);
                this.renderQueue.push (new ClearTransform ());
            }
        }, esta);
};

NodeParser.prototype.paint = function (container) {
    tratar {
        if (container instanceof ClearTransform) {
            this.renderer.ctx.restore ();
        } else if (isTextNode (contenedor)) {
            if (isPseudoElement (container.parent)) {
                container.parent.appendToDOM ();
            }
            this.paintText (contenedor);
            if (isPseudoElement (container.parent)) {
                container.parent.cleanDOM ();
            }
        } else {
            this.paintNode (contenedor);
        }
    } atrapar (e) {
        log (e);
        if (this.options.strict) {
            lanzar e;
        }
    }
};

NodeParser.prototype.paintNode = function (container) {
    if (isStackingContext (container)) {
        this.renderer.setOpacity (container.opacity);
        this.renderer.ctx.save ();
        if (container.hasTransform ()) {
            this.renderer.setTransform (container.parseTransform ());
        }
    }

    if (container.node.nodeName === "INPUT" && container.node.type === "checkbox") {
        this.paintCheckbox (contenedor);
    } else if (container.node.nodeName === "INPUT" && container.node.type === "radio") {
        this.paintRadio (contenedor);
    } else {
        this.paintElement (contenedor);
    }
};

NodeParser.prototype.paintElement = function (container) {
    l�mites de var = container.parseBounds ();
    this.renderer.clip (container.backgroundClip, function () {
        this.renderer.renderBackground (container, limits, container.borders.borders.map (getWidth));
    }, esta);

    this.renderer.clip (container.clip, function () {
        this.renderer.renderBorders (container.borders.borders);
    }, esta);

    this.renderer.clip (container.backgroundClip, function () {
        switch (container.node.nodeName) {
        caso "svg":
        caso "IFRAME":
            var imgContainer = this.images.get (container.node);
            if (imgContainer) {
                this.renderer.renderImage (contenedor, l�mites, container.borders, imgContainer);
            } else {
                log ("Error al cargar <" + container.node.nodeName + ">", container.node);
            }
            descanso;
        caso "IMG":
            var imageContainer = this.images.get (container.node.src);
            if (imageContainer) {
                this.renderer.renderImage (contenedor, l�mites, container.borders, imageContainer);
            } else {
                log ("Error al cargar <img>", container.node.src);
            }
            descanso;
        caso "LONA":
            this.renderer.renderImage (contenedor, l�mites, container.borders, {image: container.node});
            descanso;
        caso "SELECT":
        caso "ENTRADA":
        caso "TEXTAREA":
            this.paintFormValue (contenedor);
            descanso;
        }
    }, esta);
};

NodeParser.prototype.paintCheckbox = function (container) {
    var b = container.parseBounds ();

    var size = Math.min (b.width, b.height);
    l�mites var = {ancho: tama�o - 1, altura: tama�o - 1, parte superior: b.top, izquierda: b.left};
    var r = [3, 3];
    var radio = [r, r, r, r];
    bordes de var = [1,1,1,1] .map (funci�n (w) {
        return {color: new Color ('# A5A5A5'), width: w};
    });

    var borderPoints = calculaCurvePoints (l�mites, radio, bordes);

    this.renderer.clip (container.backgroundClip, function () {
        this.renderer.rectangle (lines.left + 1, lines.top + 1, lines.width - 2, lines.height - 2, new Color ("# DEDEDE"));
        this.renderer.renderBorders (calculaBorders (bordes, l�mites, borderPoints, radio));
        if (container.node.checked) {
            this.renderer.font (nuevo Color ('# 424242'), 'normal', 'normal', 'negrita', (tama�o - 3) + "px", 'arial');
            this.renderer.text ("\ u2714" ,mites.left + tama�o / 6, l�mites.top + tama�o - 1);
        }
    }, esta);
};

NodeParser.prototype.paintRadio = function (container) {
    l�mites de var = container.parseBounds ();

    var size = Math.min (lines.width, lines.height) - 2;

    this.renderer.clip (container.backgroundClip, function () {
        this.renderer.circleStroke (lines.left + 1, lines.top + 1, size, new Color ('# DEDEDE'), 1, new Color ('# A5A5A5'));
        if (container.node.checked) {
            this.renderer.circle (Math.ceil (fronteras.left + tama�o / 4) + 1, Math.ceil (fronteras.top + tama�o / 4) + 1, Math.floor (tama�o / 2), nuevo Color ('# 424242 '));
        }
    }, esta);
};

NodeParser.prototype.paintFormValue = function (container) {
    var value = container.getValue ();
    if (value.length> 0) {
        var document = container.node.ownerDocument;
        var wrapper = document.createElement ('html2canvaswrapper');
        propiedades var = ['lineHeight', 'textAlign', 'fontFamily', 'fontWeight', 'fontSize', 'color',
            'paddingLeft', 'paddingTop', 'paddingRight', 'paddingBottom',
            'width', 'height', 'borderLeftStyle', 'borderTopStyle', 'borderLeftWidth', 'borderTopWidth',
            'boxSizing', 'whiteSpace', 'wordWrap'];

        properties.forEach (function (propiedad) {
            tratar {
                wrapper.style [propiedad] = container.css (propiedad);
            } atrapar (e) {
                // El IE antiguo tiene problemas con el "borde"
                log ("html2canvas: Parse: excepci�n capturada en renderFormValue:" + e.message);
            }
        });
        l�mites de var = container.parseBounds ();
        wrapper.style.position = "arreglado";
        wrapper.style.left = fronteras.left + "px";
        wrapper.style.top = lines.top + "px";
        wrapper.textContent = valor;
        document.body.appendChild (wrapper);
        this.paintText (nuevo TextContainer (wrapper.firstChild, contenedor));
        document.body.removeChild (wrapper);
    }
};

NodeParser.prototype.paintText = function (container) {
    container.applyTextTransform ();
    caracteres var = window.html2canvas.punycode.ucs2.decode (container.node.data);
    var textList = (! this.options.letterRendering || noLetterSpacing (container)) &&! hasUnicode (container.node.data)? getWords (caracteres): characters.map (funci�n (car�cter) {
        devolver window.html2canvas.punycode.ucs2.encode ([car�cter]);
    });

    peso var = container.parent.fontWeight ();
    var size = container.parent.css ('fontSize');
    var family = container.parent.css ('fontFamily');
    var shadows = container.parent.parseTextShadows ();

    this.renderer.font (container.parent.color ('color'), container.parent.css ('fontStyle'), container.parent.css ('fontVariant'), peso, tama�o, familia);
    if (shadows.length) {
        // TODO: soporta m�ltiples sombras de texto
        this.renderer.fontShadow (sombras [0]. color, sombras [0] .offsetX, sombras [0] .offsetY, sombras [0] .blur);
    } else {
        this.renderer.clearShadow ();
    }

    this.renderer.clip (container.parent.clip, function () {
        textList.map (this.parseTextBounds (contenedor), this) .forEach (funci�n (l�mites, �ndice) {
            si (l�mites) {
                this.renderer.text (textList [index], lines.left, lines.bottom);
                this.renderTextDecoration (container.parent, limits, this.fontMetrics.getMetrics (familia, tama�o));
            }
        }, esta);
    }, esta);
};

NodeParser.prototype.renderTextDecoration = funci�n (contenedor, l�mites, m�tricas) {
    switch (container.css ("textDecoration"). split ("") [0]) {
    caso "subrayado":
        // Dibuja una l�nea en la l�nea de base de la fuente
        // TODO como algunos navegadores muestran la l�nea como m�s de 1px si el tama�o de la fuente es grande, debe tenerlo en cuenta tanto en la posici�n como en el tama�o
        this.renderer.rectangle (lines.left, Math.round (limits.top + metrics.baseline + metrics.lineWidth), lines.width, 1, container.color ("color"));
        descanso;
    caso "overline":
        this.renderer.rectangle (fronteras.left, Math.round (fronteras.top), l�mites.width, 1, contenedor.color ("color"));
        descanso;
    caso "line-through":
        // TODO tratar de encontrar la posici�n exacta para la l�nea de paso
        this.renderer.rectangle (limits.left, Math.ceil (limits.top + metrics.middle + metrics.lineWidth), lines.width, 1, container.color ("color"));
        descanso;
    }
};

var borderColorTransforms = {
    recuadro: [
        ["oscurecer", 0.60],
        ["oscurecer", 0.10],
        ["oscurecer", 0.10],
        ["oscurecer", 0.60]
    ]
};

NodeParser.prototype.parseBorders = function (container) {
    var nodeBounds = container.parseBounds ();
    var radius = getBorderRadiusData (contenedor);
    var bordes = ["Arriba", "Derecha", "Abajo", "Izquierda"]. map (funci�n (lado, �ndice) {
        var style = container.css ('border' + side + 'Style');
        var color = container.color ('border' + side + 'Color');
        if (style === "inset" && color.isBlack ()) {
            color = nuevo Color ([255, 255, 255, color.a]); // esto est� mal, pero
        }
        var colorTransform = borderColorTransforms [estilo]? borderColorTransforms [estilo] [�ndice]: nulo;
        regreso {
            width: container.cssInt ('border' + side + 'Width'),
            color: colorTransform? color [colorTransform [0]] (colorTransform [1]): color,
            Args: nulo
        };
    });
    var borderPoints = calculaCurvePoints (nodeBounds, radio, bordes);

    regreso {
        clip: this.parseBackgroundClip (contenedor, borderPoints, bordes, radio, nodeBounds),
        bordes: calculaBorders (bordes, nodeBounds, borderPoints, radio)
    };
};

funci�n calculaBorders (bordes, nodeBounds, borderPoints, radio) {
    retorne border.map (funci�n (border, borderSide) {
        if (border.width> 0) {
            var bx = nodeBounds.left;
            var por = nodeBounds.top;
            var bw = nodeBounds.width;
            var bh = nodeBounds.height - (bordes [2] .width);

            switch (borderSide) {
            caso 0:
                // borde superior
                bh = bordes [0] .width;
                border.args = drawSide ({
                        c1: [bx, por],
                        c2: [bx + bw, por],
                        c3: [bx + bw - bordes [1] .width, por + bh],
                        c4: [bx + bordes [3] .width, por + bh]
                    }, radio [0], radio [1],
                    borderPoints.topLeftOuter, borderPoints.topLeftInner, borderPoints.topRightOuter, borderPoints.topRightInner);
                descanso;
            caso 1:
                // borde derecho
                bx = nodeBounds.left + nodeBounds.width - (bordes [1] .width);
                bw = bordes [1] .width;

                border.args = drawSide ({
                        c1: [bx + bw, por],
                        c2: [bx + bw, por + bh + bordes [2] .width],
                        c3: [bx, por + bh],
                        c4: [bx, por + bordes [0] .width]
                    }, radio [1], radio [2],
                    borderPoints.topRightOuter, borderPoints.topRightInner, borderPoints.bottomRightOuter, borderPoints.bottomRightInner);
                descanso;
            caso 2:
                // borde inferior
                por = (por + nodeBounds.height) - (bordes [2] .width);
                bh = bordes [2] .width;
                border.args = drawSide ({
                        c1: [bx + bw, por + bh],
                        c2: [bx, por + bh],
                        c3: [bx + bordes [3] .width, por],
                        c4: [bx + bw - bordes [3] .width, por]
                    }, radio [2], radio [3],
                    borderPoints.bottomRightOuter, borderPoints.bottomRightInner, borderPoints.bottomLeftOuter, borderPoints.bottomLeftInner);
                descanso;
            caso 3:
                // borde izquierdo
                bw = bordes [3] .width;
                border.args = drawSide ({
                        c1: [bx, por + bh + bordes [2] .width],
                        c2: [bx, por],
                        c3: [bx + bw, por + bordes [0] .width],
                        c4: [bx + bw, por + bh]
                    }, radio [3], radio [0],
                    borderPoints.bottomLeftOuter, borderPoints.bottomLeftInner, borderPoints.topLeftOuter, borderPoints.topLeftInner);
                descanso;
            }
        }
        frontera de retorno
    });
}

NodeParser.prototype.parseBackgroundClip = function (contenedor, borderPoints, bordes, radio, l�mites) {
    var backgroundClip = container.css ('backgroundClip'),
        borderArgs = [];

    interruptor (backgroundClip) {
    caso "caja de contenido":
    caso "caja de relleno":
        parseCorner (borderArgs, radio [0], radio [1], borderPoints.topLeftInner, borderPoints.topRightInner, limits.left + confines [3] .width, fronteras.top + fronteras [0] .width);
        parseCorner (borderArgs, radio [1], radio [2], borderPoints.topRightInner, borderPoints.bottomRightInner, limits.left + limits.width - fronteras [1] .width, fronteras.top + fronteras [0] .width);
        parseCorner (borderArgs, radio [2], radio [3], borderPoints.bottomRightInner, borderPoints.bottomLeftInner, limits.left + limits.width - fronteras [1] .width, lines.top + lines.height - fronteras [2]. anchura);
        parseCorner (borderArgs, radio [3], radio [0], borderPoints.bottomLeftInner, borderPoints.topLeftInner, limits.left + confines [3] .width, fronteras.top + fronteras.height - fronteras [2] .width));
        descanso;

    defecto:
        parseCorner (borderArgs, radio [0], radio [1], borderPoints.topLeftOuter, borderPoints.topRightOuter, limits.left, lines.top);
        parseCorner (borderArgs, radius [1], radius [2], borderPoints.topRightOuter, borderPoints.bottomRightOuter, limits.left + lines.width, lines.top);
        parseCorner (borderArgs, radio [2], radio [3], borderPoints.bottomRightOuter, borderPoints.bottomLeftOuter, limits.left + lines.width, limits.top + lines.height);
        parseCorner (borderArgs, radius [3], radius [0], borderPoints.bottomLeftOuter, borderPoints.topLeftOuter, lines.left, limits.top + lines.height);
        descanso;
    }

    volver borderArgs;
};

funci�n getCurvePoints (x, y, r1, r2) {
    var kappa = 4 * ((Math.sqrt (2) - 1) / 3);
    var ox = (r1) * kappa, // punto de control desplazado horizontal
        oy = (r2) * kappa, // punto de control desplazado vertical
        xm = x + r1, // x-middle
        ym = y + r2; // y-middle
    regreso {
        topLeft: bezierCurve ({x: x, y: ym}, {x: x, y: ym - oy}, {x: xm - ox, y: y}, {x: xm, y: y}),
        topRight: bezierCurve ({x: x, y: y}, {x: x + ox, y: y}, {x: xm, y: ym - oy}, {x: xm, y: ym}),
        bottomRight: bezierCurve ({x: xm, y: y}, {x: xm, y: y + oy}, {x: x + ox, y: ym}, {x: x, y: ym}),
        bottomLeft: bezierCurve ({x: xm, y: ym}, {x: xm - ox, y: ym}, {x: x, y: y + oy}, {x: x, y: y})
    };
}

funci�n calculaCurvePoints (bordes, borderRadius, bordes) {
    var x = fronteras.left,
        y = fronteras.top,
        ancho = l�mites. ancho
        altura = l�mites. altura

        tlh = borderRadius [0] [0],
        tlv = borderRadius [0] [1],
        trh = borderRadius [1] [0],
        trv = borderRadius [1] [1],
        brh = borderRadius [2] [0],
        brv = borderRadius [2] [1],
        blh = borderRadius [3] [0],
        blv = borderRadius [3] [1];

    var topWidth = ancho - trh,
        rightHeight = altura - brv,
        bottomWidth = ancho - brh,
        LeftHeight = altura - blv;

    regreso {
        topLeftOuter: getCurvePoints (x, y, tlh, tlv) .topLeft.subdivide (0.5),
        topLeftInner: getCurvePoints (x + bordes [3] .width, y + fronteras [0] .width, Math.max (0, tlh - fronteras [3] .width), Math.max (0, tlv - fronteras [0] .width)). topLeft.subdivide (0.5),
        topRightOuter: getCurvePoints (x + topWidth, y, trh, trv) .topRight.subdivide (0.5),
        topRightInner: getCurvePoints (x + Math.min (topWidth, width + fronteras [3] .width), y + bordes [0] .width, (topWidth> width + fronteras [3] .width)? 0: trh - fronteras [ 3] .width, trv - fronteras [0] .width) .topRight.subdivide (0.5),
        bottomRightOuter: getCurvePoints (x + bottomWidth, y + rightHeight, brh, brv) .bottomRight.subdivide (0.5),
        bottomRightInner: getCurvePoints (x + Math.min (bottomWidth, width - fronteras [3] .width), y + Math.min (rightHeight, height + bordes [0] .width), Math.max (0, brh - fronteras [ 1] .width), brv - bordes [2] .width) .bottomRight.subdivide (0.5),
        bottomLeftOuter: getCurvePoints (x, y + leftHeight, blh, blv) .bottomLeft.subdivide (0.5),
        bottomLeftInner: getCurvePoints (x + fronteras [3] .width, y + leftHeight, Math.max (0, blh - fronteras [3] .width), blv - fronteras [2] .width) .bottomLeft.subdivide (0.5)
    };
}

function bezierCurve (start, startControl, endControl, end) {
    var lerp = funci�n (a, b, t) {
        regreso {
            x: ax + (bx - ax) * t,
            y: ay + (by - ay) * t
        };
    };

    regreso {
        inicio: inicio
        startControl: startControl,
        endControl: endControl,
        fin
        subdivide: funci�n (t) {
            var ab = lerp (start, startControl, t),
                bc = lerp (startControl, endControl, t),
                cd = lerp (endControl, end, t),
                abbc = lerp (ab, bc, t),
                bccd = lerp (bc, cd, t),
                dest = lerp (abbc, bccd, t);
            return [bezierCurve (inicio, ab, abbc, dest), bezierCurve (dest, bccd, cd, final)];
        }
        curveTo: function (borderArgs) {
            borderArgs.push (["bezierCurve", startControl.x, startControl.y, endControl.x, endControl.y, end.x, end.y]);
        }
        curveToReversed: function (borderArgs) {
            borderArgs.push (["bezierCurve", endControl.x, endControl.y, startControl.x, startControl.y, start.x, start.y]);
        }
    };
}

funci�n drawSide (borderData, radio1, radio2, exterior1, interior1, exterior2, interno2) {
    var borderArgs = [];

    if (radius1 [0]> 0 || radius1 [1]> 0) {
        borderArgs.push (["line", outer1 [1] .start.x, outer1 [1] .start.y]);
        outer1 [1] .curveTo (borderArgs);
    } else {
        borderArgs.push (["line", borderData.c1 [0], borderData.c1 [1]]);
    }

    if (radius2 [0]> 0 || radius2 [1]> 0) {
        borderArgs.push (["line", outer2 [0] .start.x, outer2 [0] .start.y]);
        outer2 [0] .curveTo (borderArgs);
        borderArgs.push (["line", inner2 [0] .end.x, inner2 [0] .end.y]);
        inner2 [0] .curveToReversed (borderArgs);
    } else {
        borderArgs.push (["line", borderData.c2 [0], borderData.c2 [1]]);
        borderArgs.push (["line", borderData.c3 [0], borderData.c3 [1]]);
    }

    if (radius1 [0]> 0 || radius1 [1]> 0) {
        borderArgs.push (["line", inner1 [1] .end.x, inner1 [1] .end.y]);
        inner1 [1] .curveToReversed (borderArgs);
    } else {
        borderArgs.push (["line", borderData.c4 [0], borderData.c4 [1]]);
    }

    volver borderArgs;
}

funci�n parseCorner (borderArgs, radius1, radius2, corner1, corner2, x, y) {
    if (radius1 [0]> 0 || radius1 [1]> 0) {
        borderArgs.push (["line", corner1 [0] .start.x, corner1 [0] .start.y]);
        corner1 [0] .curveTo (borderArgs);
        corner1 [1] .curveTo (borderArgs);
    } else {
        borderArgs.push (["line", x, y]);
    }

    if (radius2 [0]> 0 || radius2 [1]> 0) {
        borderArgs.push (["line", corner2 [0] .start.x, corner2 [0] .start.y]);
    }
}

funci�n negativeZIndex (contenedor) {
    devolver container.cssInt ("zIndex") <0;
}

funci�n positiveZIndex (contenedor) {
    return container.cssInt ("zIndex")> 0;
}

funci�n zIndex0 (contenedor) {
    return container.cssInt ("zIndex") === 0;
}

funci�n inlineLevel (contenedor) {
    devuelve ["inline", "inline-block", "inline-table"]. indexOf (container.css ("display"))! == -1;
}

funci�n isStackingContext (contenedor) {
    return (container instanceof StackingContext);
}

funci�n hasText (contenedor) {
    devolver container.node.data.trim (). length> 0;
}

funci�n noLetterSpacing (contenedor) {
    return (/^(normal|none|0px)$/.test(container.parent.css("letterSpacing ")));
}

funci�n getBorderRadiusData (contenedor) {
    return ["TopLeft", "TopRight", "BottomRight", "BottomLeft"]. map (function (side) {
        var value = container.css ('border' + side + 'Radius');
        var arr = value.split ("");
        si (arr.length <= 1) {
            arr [1] = arr [0];
        }
        volver arr.map (asInt);
    });
}

funci�n renderableNode (nodo) {
    return (node.nodeType === Node.TEXT_NODE || node.nodeType === Node.ELEMENT_NODE);
}

funci�n isPositionedForStacking (contenedor) {
    var position = container.css ("position");
    var zIndex = (["absolute", "relative", "fixed"]. indexOf (position)! == -1)? container.css ("zIndex"): "auto";
    devuelve zIndex! == "auto";
}

funci�n est� posicionada (contenedor) {
    return container.css ("position")! == "static";
}

funci�n isFloating (contenedor) {
    return container.css ("float")! == "none";
}

funci�n isInlineBlock (contenedor) {
    devuelve ["inline-block", "inline-table"]. indexOf (container.css ("display"))! == -1;
}

funci�n no (devoluci�n de llamada) {
    contexto var = esto;
    funci�n de retorno () {
        return! callback.apply (contexto, argumentos);
    };
}

funci�n isElement (contenedor) {
    devolver container.node.nodeType === Node.ELEMENT_NODE;
}

funci�n isPseudoElement (contenedor) {
    return container.isPseudoElement === true;
}

funci�n isTextNode (contenedor) {
    devolver container.node.nodeType === Node.TEXT_NODE;
}

funci�n zIndexSort (contextos) {
    funci�n de retorno (a, b) {
        return (a.cssInt ("zIndex") + (contexts.indexOf (a) / contexts.length)) - (b.cssInt ("zIndex") + (contexts.indexOf (b) / contexts.length));
    };
}

funci�n hasOpacity (contenedor) {
    return container.getOpacity () <1;
}

funci�n de enlace (devoluci�n de llamada, contexto) {
    funci�n de retorno () {
        devolver callback.apply (contexto, argumentos);
    };
}

funci�n asInt (valor) {
    devuelve parseInt (valor, 10);
}

funci�n getWidth (border) {
    volver border.width;
}

funci�n nonIgnoredElement (nodeContainer) {
    return (nodeContainer.node.nodeType! == Node.ELEMENT_NODE || ["SCRIPT", "HEAD", "TITLE", "OBJECT", "BR", "OPTION"]. indexOf (nodeContainer.node.nodeName) = == -1);
}

funci�n de aplanar (matrices) {
    devolver [] .concat.apply ([], matrices);
}

funci�n stripQuotes (contenido) {
    var primero = content.substr (0, 1);
    return (first === content.substr (content.length - 1) && first.match (/ '| "/)) content.substr (1, content.length - 2): content;
}

funci�n getWords (caracteres) {
    var palabras = [], i = 0, onWordBoundary = false, palabra;
    while (caracteres.longitud) {
        if (isWordBoundary (caracteres [i]) === onWordBoundary) {
            palabra = caracteres.splice (0, i);
            if (word.length) {
                words.push (window.html2canvas.punycode.ucs2.encode (word));
            }
            onWordBoundary =! enWordBordary;
            i = 0;
        } else {
            i ++;
        }

        if (i> = characters.length) {
            palabra = caracteres.splice (0, i);
            if (word.length) {
                words.push (window.html2canvas.punycode.ucs2.encode (word));
            }
        }
    }
    devolver palabras;
}

funci�n isWordBoundary (characterCode) {
    regreso [
        32, // <espacio>
        13, // \ r
        10, // \ n
        9, // \ t
        45 // -
    ] .indexOf (characterCode)! == -1;
}

funci�n hasUnicode (cadena) {
    return (/[^\u0000-\u00ff?/).test(string);
}

funci�n Proxy (src, proxyUrl, documento) {
    if (! proxyUrl) {
        devolver Promise.reject ("No hay proxy configurado");
    }
    var callback = createCallback (supportCORS);
    var url = createProxyUrl (proxyUrl, src, callback);

    devuelve supportCORS? XHR (url): (jsonp (document, url, callback) .then (function (response) {
        return decode64 (response.content);
    }));
}
var proxyCount = 0;

var supportCORS = ('withCredentials' en el nuevo XMLHttpRequest ());
var apoyaCORSImage = ('crossOrigin' en new Image ());

funci�n ProxyURL (src, proxyUrl, documento) {
    var callback = createCallback (supportCORSImage);
    var url = createProxyUrl (proxyUrl, src, callback);
    return (supportCORSImage? Promise.resolve (url): jsonp (document, url, callback) .then (function (response) {
        devuelve "data:" + response.type + "; base64," + response.content;
    }));
}

funci�n jsonp (documento, url, devoluci�n de llamada) {
    devolver nueva promesa (funci�n (resolver, rechazar) {
        var s = document.createElement ("script");
        var cleanup = function () {
            eliminar window.html2canvas.proxy [callback];
            document.body.removeChild (s);
        };
        window.html2canvas.proxy [callback] = function (response) {
            limpiar();
            resolver (respuesta);
        };
        s.src = url;
        s.onerror = function (e) {
            limpiar();
            rechazar (e);
        };
        document.body.appendChild (s);
    });
}

funci�n createCallback (useCORS) {
    volver! useCORS? "html2canvas_" + Date.now () + "_" + (++ proxyCount) + "_" + Math.round (Math.random () * 100000): "";
}

funci�n createProxyUrl (proxyUrl, src, callback) {
    devuelve proxyUrl + "? url =" + encodeURIComponent (src) + (callback.length? "& callback = html2canvas.proxy." + callback: "");
}

funci�n ProxyImageContainer (src, proxy) {
    var script = document.createElement ("script");
    var link = document.createElement ("a");
    link.href = src;
    src = link.href;
    this.src = src;
    this.image = new Image ();
    var self = esto;
    this.promise = new Promise (funci�n (resolver, rechazar) {
        self.image.crossOrigin = "An�nimo";
        self.image.onload = resolver;
        self.image.onerror = rechazar;

        nuevo ProxyURL (src, proxy, documento) .then (function (url) {
            self.image.src = url;
        }) ['captura'] (rechazar);
    });
}

funci�n PseudoElementContainer (nodo, padre, tipo) {
    NodeContainer.call (este, nodo, padre);
    this.isPseudoElement = true;
    this.before = type === ": before";
}

PseudoElementContainer.prototype.cloneTo = function (stack) {
    PseudoElementContainer.prototype.cloneTo.call (this, stack);
    stack.isPseudoElement = true;
    stack.before = this.before;
};

PseudoElementContainer.prototype = Object.create (NodeContainer.prototype);

PseudoElementContainer.prototype.appendToDOM = function () {
    if (this.before) {
        this.parent.node.insertBefore (this.node, this.parent.node.firstChild);
    } else {
        this.parent.node.appendChild (this.node);
    }
    this.parent.node.className + = "" + this.getHideClass ();
};

PseudoElementContainer.prototype.cleanDOM = function () {
    this.node.parentNode.removeChild (this.node);
    this.parent.node.className = this.parent.node.className.replace (this.getHideClass (), "");
};

PseudoElementContainer.prototype.getHideClass = function () {
    devuelve este ["PSEUDO_HIDE_ELEMENT_CLASS_" + (this.before? "ANTES de": "AFTER")];
};

PseudoElementContainer.prototype.PSEUDO_HIDE_ELEMENT_CLASS_BEFORE = "___html2canvas___pseudoelement_before";
PseudoElementContainer.prototype.PSEUDO_HIDE_ELEMENT_CLASS_AFTER = "___html2canvas___pseudoelement_after";

Funci�n Renderer (ancho, alto, im�genes, opciones, documento) {
    this.width = ancho;
    this.height = altura;
    this.images = im�genes;
    this.options = opciones;
    this.document = document;
}

Renderer.prototype.renderImage = function (container ,mites, borderData, imageContainer) {
    var paddingLeft = container.cssInt ('paddingLeft'),
        paddingTop = container.cssInt ('paddingTop'),
        paddingRight = container.cssInt ('paddingRight'),
        paddingBottom = container.cssInt ('paddingBottom'),
        fronteras = borderData.borders;

    var ancho = l�mites.width - (bordes [1] .width + bordes [3] .width + paddingLeft + paddingRight);
    var altura = l�mites.height - (bordes [0] .width + fronteras [2] .width + paddingTop + paddingBottom);
    this.drawImage (
        contenedor de imagen,
        0,
        0,
        imageContainer.image.width || anchura,
        imageContainer.image.height || altura,
        fronteras.left + paddingLeft + fronteras [3] .width,
        fronteras.top + paddingTop + fronteras [0] .width,
        anchura,
        altura
    );
};

Renderer.prototype.renderBackground = function (container ,mites, borderData) {
    if (lines.height> 0 && limits.width> 0) {
        this.renderBackgroundColor (contenedor, l�mites);
        this.renderBackgroundImage (container, limits, borderData);
    }
};

Renderer.prototype.renderBackgroundColor = funci�n (contenedor, l�mites) {
    var color = container.color ("backgroundColor");
    if (! color.isTransparent ()) {
        this.rectangle (fronteras.left, l�mites.top, l�mites.width, l�mites.height, color);
    }
};

Renderer.prototype.renderBorders = funci�n (bordes) {
    fronteras.para cada (esto.renderBorder, esto);
};

Renderer.prototype.renderBorder = function (data) {
    if (! data.color.isTransparent () && data.args! == null) {
        this.drawShape (data.args, data.color);
    }
};

Renderer.prototype.renderBackgroundImage = function (container ,mites, borderData) {
    var backgroundImages = container.parseBackgroundImages ();
    backgroundImages.reverse (). forEach (function (backgroundImage, index, arr) {
        interruptor (backgroundImage.method) {
        caso "url":
            var image = this.images.get (backgroundImage.args [0]);
            if (imagen) {
                this.renderBackgroundRepeating (contenedor, l�mites, imagen, arr.length - (�ndice + 1), borderData);
            } else {
                log ("Error al cargar la imagen de fondo", backgroundImage.args [0]);
            }
            descanso;
        caso "lineal-gradiente":
        caso "gradiente":
            var gradientImage = this.images.get (backgroundImage.value);
            if (gradientImage) {
                this.renderBackgroundGradient (gradientImage, bound, borderData);
            } else {
                log ("Error al cargar la imagen de fondo", backgroundImage.args [0]);
            }
            descanso;
        caso "ninguno":
            descanso;
        defecto:
            log ("Tipo de imagen de fondo desconocido", backgroundImage.args [0]);
        }
    }, esta);
};

Renderer.prototype.renderBackgroundRepeating = funci�n (contenedor, l�mites, imageContainer, index, borderData) {
    var size = container.parseBackgroundSize (lines, imageContainer.image, index);
    var position = container.parseBackgroundPosition (lines, imageContainer.image, index, size);
    var repeat = container.parseBackgroundRepeat (�ndice);
    cambiar (repetir) {
    caso "repetir-x":
    caso "repetir no repetir":
        this.backgroundRepeatShape (imageContainer, position, size ,mites, bound.left + borderData [3], lines.top + position.top + borderData [0], 99999, size.height, borderData);
        descanso;
    caso "repetir-y":
    caso "no repetir la repetici�n":
        this.backgroundRepeatShape (imageContainer, posici�n, tama�o, l�mites, fronteras.left + posici�n.left + fronteraDatos [3], l�mites.top + fronteraDatos [0], tama�o.alibre, 99999, fronteraDatos);
        descanso;
    caso "no-repetir":
        this.backgroundRepeatShape (imageContainer, position, size ,mites, bound.left + position.left + borderData [3], limits.top + position.top + borderData [0], size.width, size.height, borderData);
        descanso;
    defecto:
        this.renderBackgroundRepeat (imageContainer, position, size, {top: lines.top, left: lines.left}, borderData [3], borderData [0]);
        descanso;
    }
};

funci�n StackingContext (hasOwnStacking, opacity, element, parent) {
    NodeContainer.call (this, element, parent);
    this.ownStacking = hasOwnStacking;
    this.contexts = [];
    this.children = [];
    this.opacity = (this.parent? this.parent.stack.opacity: 1) * opacity;
}

StackingContext.prototype = Object.create (NodeContainer.prototype);

StackingContext.prototype.getParentStack = function (context) {
    var parentStack = (this.parent)? this.parent.stack: null;
    volver parentStack? (parentStack.ownStacking? parentStack: parentStack.getParentStack (contexto)): context.stack;
};

Funci�n de soporte (documento) {
    this.rangeBounds = this.testRangeBounds (documento);
    this.cors = this.testCORS ();
    this.svg = this.testSVG ();
}

Support.prototype.testRangeBounds = function (document) {
    var range, testElement, rangeBounds, rangeHeight, support = false;

    if (document.createRange) {
        range = document.createRange ();
        if (range.getBoundingClientRect) {
            testElement = document.createElement ('boundtest');
            testElement.style.height = "123px";
            testElement.style.display = "block";
            document.body.appendChild (testElement);

            range.selectNode (testElement);
            rangeBounds = range.getBoundingClientRect ();
            rangeHeight = rangeBounds.height;

            if (rangeHeight === 123) {
                support = true;
            }
            document.body.removeChild (testElement);
        }
    }

    apoyo de retorno;
};

Support.prototype.testCORS = function () {
    devuelva typeof ((new Image ()). crossOrigin)! == "undefined";
};

Support.prototype.testSVG = function () {
    var img = nueva imagen ();
    var canvas = document.createElement ("canvas");
    var ctx = canvas.getContext ("2d");
    img.src = "datos: imagen / svg + xml, <svg xmlns = 'http: //www.w3.org/2000/svg'> </svg>";

    tratar {
        ctx.drawImage (img, 0, 0);
        canvas.toDataURL ();
    } atrapar (e) {
        falso retorno;
    }
    devuelve verdadero
};

funci�n SVGContainer (src) {
    this.src = src;
    this.image = null;
    var self = esto;

    this.promise = this.hasFabric (). then (function () {
        return (self.isInline (src)? Promise.resolve (self.inlineFormatting (src)): XHR (src));
    }). entonces (funci�n (svg) {
        devolver nueva promesa (funci�n (resolver) {
            html2canvas.fabric.loadSVGFromString (svg, self.createCanvas.call (self, resolver));
        });
    });
}

SVGContainer.prototype.hasFabric = function () {
    volver! html2canvas.fabric? Promise.reject (nuevo error ("html2canvas.svg.js no est� cargado, no se puede procesar svg")): Promise.resolve ();
};

SVGContainer.prototype.inlineFormatting = function (src) {
    return (/^data:image\/svg\+xml;base64,/.test(src))? this.decode64 (this.removeContentType (src)): this.removeContentType (src);
};

SVGContainer.prototype.removeContentType = function (src) {
    devuelve src.replace (/ ^ data: image \ / svg \ + xml (; base64)?, /, '');
};

SVGContainer.prototype.isInline = function (src) {
    return (/^data:image\/svg\+xml/i.test(src));
};

SVGContainer.prototype.createCanvas = function (resolver) {
    var self = esto;
    Funci�n de retorno (objetos, opciones) {
        var canvas = new html2canvas.fabric.StaticCanvas ('c');
        self.image = canvas.lowerCanvasEl;
        lona
            .setWidth (options.width)
            .setHeight (options.height)
            .add (html2canvas.fabric.util.groupSVGElements (objetos, opciones))
            .renderAll ();
        resolver (canvas.lowerCanvasEl);
    };
};

SVGContainer.prototype.decode64 = function (str) {
    return (typeof (window.atob) === "function")? window.atob (str): decode64 (str);
};

/ *
 * base64-arraybuffer
 * https://github.com/niklasvh/base64-arraybuffer
 *
 * Copyright (c) 2012 Niklas von Hertzen
 * Licenciado bajo la licencia MIT.
 * /

funci�n decode64 (base64) {
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 + /";
    var len = base64.length, i, encoded1, encoded2, encoded3, encoded4, byte1, byte2, byte3;

    var output = "";

    para (i = 0; i <len; i + = 4) {
        encoded1 = chars.indexOf (base64 [i]);
        encoded2 = chars.indexOf (base64 [i + 1]);
        encoded3 = chars.indexOf (base64 [i + 2]);
        encoded4 = chars.indexOf (base64 [i + 3]);

        byte1 = (codificado1 << 2) | (codificado2 >> 4);
        byte2 = ((codificado2 y 15) << 4) | (codificado3 >> 2);
        byte3 = ((codificado 3 y 3) << 6) | codificado4;
        if (codificado3 === 64) {
            output + = String.fromCharCode (byte1);
        } else if (codificado4 === 64 || codificado4 === -1) {
            output + = String.fromCharCode (byte1, byte2);
        } else {
            output + = String.fromCharCode (byte1, byte2, byte3);
        }
    }

    retorno de salida;
}

funci�n SVGNodeContainer (nodo, nativo) {
    this.src = nodo;
    this.image = null;
    var self = esto;

    esto.promiso = nativo? nueva promesa (funci�n (resolver, rechazar) {
        self.image = new Image ();
        self.image.onload = resolver;
        self.image.onerror = rechazar;
        self.image.src = "data: image / svg + xml," + (nuevo XMLSerializer ()). serializeToString (node);
        if (self.image.complete === true) {
            resolver (self.image);
        }
    }): this.hasFabric (). then (function () {
        devolver nueva promesa (funci�n (resolver) {
            html2canvas.fabric.parseSVGDocument (node, self.createCanvas.call (self, resolver));
        });
    });
}

SVGNodeContainer.prototype = Object.create (SVGContainer.prototype);

funci�n TextContainer (nodo, padre) {
    NodeContainer.call (este, nodo, padre);
}

TextContainer.prototype = Object.create (NodeContainer.prototype);

TextContainer.prototype.applyTextTransform = function () {
    this.node.data = this.transform (this.parent.css ("textTransform"));
};

TextContainer.prototype.transform = function (transform) {
    var text = this.node.data;
    interruptor (transformar) {
        caso "min�scula":
            devuelve text.toLowerCase ();
        caso "capitalizar":
            devuelve text.replace (/ (^ | \ s |: | - | \ (| \)) ([az]) / g, may�scula);
        caso "may�scula":
            devuelve text.toUpperCase ();
        defecto:
            texto de vuelta
    }
};

funci�n may�scula (m, p1, p2) {
    if (m.length> 0) {
        devuelve p1 + p2.toUpperCase ();
    }
}

funci�n WebkitGradientContainer (imageData) {
    GradientContainer.apply (esto, argumentos);
    this.type = (imageData.args [0] === "linear")? this.TYPES.LINEAR: this.TYPES.RADIAL;
}

WebkitGradientContainer.prototype = Object.create (GradientContainer.prototype);

funci�n XHR (url) {
    devolver nueva promesa (funci�n (resolver, rechazar) {
        var xhr = nuevo XMLHttpRequest ();
        xhr.open ('GET', url);

        xhr.onload = function () {
            if (xhr.status === 200) {
                resolver (xhr.responseText);
            } else {
                rechazar (nuevo Error (xhr.statusText));
            }
        };

        xhr.onerror = function () {
            rechazar (nuevo error ("Error de red"));
        };

        xhr.send ();
    });
}

Funci�n CanvasRenderer (ancho, alto) {
    Renderer.apply (esto, argumentos);
    this.canvas = this.options.canvas || this.document.createElement ("canvas");
    if (! this.options.canvas) {
        this.canvas.width = ancho;
        this.canvas.height = altura;
    }
    this.ctx = this.canvas.getContext ("2d");
    this.taintCtx = this.document.createElement ("canvas"). getContext ("2d");
    this.ctx.textBaseline = "bottom";
    this.variables = {};
    log ("Initialized CanvasRenderer with size", ancho, "x", altura);
}

CanvasRenderer.prototype = Object.create (Renderer.prototype);

CanvasRenderer.prototype.setFillStyle = function (fillStyle) {
    this.ctx.fillStyle = typeof (fillStyle) === "object" && !! fillStyle.isColor? fillStyle.toString (): fillStyle;
    devuelve this.ctx;
};

CanvasRenderer.prototype.rectangle = function (left, top, width, height, color) {
    this.setFillStyle (color) .fillRect (izquierda, arriba, ancho, altura);
};

CanvasRenderer.prototype.circle = function (left, top, size, color) {
    this.setFillStyle (color);
    this.ctx.beginPath ();
    this.ctx.arc (left + size / 2, top + size / 2, size / 2, 0, Math.PI * 2, true);
    this.ctx.closePath ();
    this.ctx.fill ();
};

CanvasRenderer.prototype.circleStroke = function (left, top, size, color, stroke, strokeColor) {
    this.circle (izquierda, arriba, tama�o, color);
    this.ctx.strokeStyle = strokeColor.toString ();
    this.ctx.stroke ();
};

CanvasRenderer.prototype.drawShape = function (shape, color) {
    esta forma (forma);
    this.setFillStyle (color) .fill ();
};

CanvasRenderer.prototype.taints = function (imageContainer) {
    if (imageContainer.tainted === null) {
        this.taintCtx.drawImage (imageContainer.image, 0, 0);
        tratar {
            this.taintCtx.getImageData (0, 0, 1, 1);
            imageContainer.tainted = false;
        } atrapar (e) {
            this.taintCtx = document.createElement ("canvas"). getContext ("2d");
            imageContainer.tainted = true;
        }
    }

    devuelve imageContainer.tainted;
};

CanvasRenderer.prototype.drawImage = function (imageContainer, sx, sy, sw, sh, dx, dy, dw, dh) {
    if (! this.taints (imageContainer) || this.options.allowTaint) {
        this.ctx.drawImage (imageContainer.image, sx, sy, sw, sh, dx, dy, dw, dh);
    }
};

CanvasRenderer.prototype.clip = function (shapes, callback, context) {
    this.ctx.save ();
    shapes.filter (hasEntries) .forEach (funci�n (forma) {
        this.shape (shape) .clip ();
    }, esta);
    callback.call (contexto);
    this.ctx.restore ();
};

CanvasRenderer.prototype.shape = function (shape) {
    this.ctx.beginPath ();
    shape.forEach (funci�n (punto, �ndice) {
        si (punto [0] === "rect") {
            this.ctx.rect.apply (this.ctx, point.slice (1));
        } else {
            this.ctx [(index === 0)? "moveTo": punto [0] + "To"] .apply (this.ctx, point.slice (1));
        }
    }, esta);
    this.ctx.closePath ();
    devuelve this.ctx;
};

CanvasRenderer.prototype.font = function (color, estilo, variante, peso, tama�o, familia) {
    this.setFillStyle (color) .font = [estilo, variante, peso, tama�o, familia] .join ("") .split (",") [0];
};

CanvasRenderer.prototype.fontShadow = function (color, offsetX, offsetY, blur) {
    this.setVariable ("shadowColor", color.toString ())
        .setVariable ("shadowOffsetY", offsetX)
        .setVariable ("shadowOffsetX", offsetY)
        .setVariable ("shadowBlur", blur);
};

CanvasRenderer.prototype.clearShadow = function () {
    this.setVariable ("shadowColor", "rgba (0,0,0,0)");
};

CanvasRenderer.prototype.setOpacity = function (opacity) {
    this.ctx.globalAlpha = opacidad;
};

CanvasRenderer.prototype.setTransform = function (transform) {
    this.ctx.translate (transform.origin [0], transform.origin [1]);
    this.ctx.transform.apply (this.ctx, transform.matrix);
    this.ctx.translate (-transform.origin [0], -transform.origin [1]);
};

CanvasRenderer.prototype.setVariable = function (propiedad, valor) {
    if (this.variables [propiedad]! == valor) {
        this.variables [propiedad] = this.ctx [propiedad] = valor;
    }

    devuelve esto
};

CanvasRenderer.prototype.text = function (texto, izquierda, abajo) {
    this.ctx.fillText (texto, izquierda, abajo);
};

CanvasRenderer.prototype.backgroundRepeatShape = function (imageContainer, backgroundPosition, tama�o, l�mites, izquierda, arriba, ancho, alto, borderData) {
    forma var = [
        ["l�nea", Math.round (izquierda), Math.round (arriba)],
        ["line", Math.round (left + width), Math.round (top)],
        ["l�nea", Math.round (izquierda + ancho), Math.round (altura + superior)],
        ["line", Math.round (izquierda), Math.round (altura + arriba)]
    ];
    this.clip ([shape], function () {
        this.renderBackgroundRepeat (imageContainer, backgroundPosition, tama�o, l�mites, borderData [3], borderData [0]);
    }, esta);
};

CanvasRenderer.prototype.renderBackgroundRepeat = function (imageContainer, backgroundPosition, tama�o, l�mites, borderLeft, borderTop) {
    var offsetX = Math.round (lines.left + backgroundPosition.left + borderLeft), offsetY = Math.round (lines.top + backgroundPosition.top + borderTop);
    this.setFillStyle (this.ctx.createPattern (this.resizeImage (imageContainer, tama�o), "repetir"));
    this.ctx.translate (offsetX, offsetY);
    this.ctx.fill ();
    this.ctx.translate (-offsetX, -offsetY);
};

CanvasRenderer.prototype.renderBackgroundGradient = function (gradientImage, bound) {
    if (gradientImage instanceof LinearGradientContainer) {
        gradiente var = this.ctx.createLinearGradient (
            l�mites.left + limites.width * gradientImage.x0,
            l�mites.top + l�mites.height * gradientImage.y0,
            l�mites.left + limites.width * gradientImage.x1,
            l�mites.top + l�mites.height * gradientImage.y1);
        gradientImage.colorStops.forEach (function (colorStop) {
            gradient.addColorStop (colorStop.stop, colorStop.color.toString ());
        });
        this.rectangle (fronteras.left, l�mites.top, l�mites.width, l�mites.height, gradiente);
    }
};

CanvasRenderer.prototype.resizeImage = function (imageContainer, size) {
    var image = imageContainer.image;
    if (image.width === size.width && image.height === size.height) {
        imagen de retorno
    }

    var ctx, canvas = document.createElement ('canvas');
    canvas.width = size.width;
    canvas.height = size.height;
    ctx = canvas.getContext ("2d");
    ctx.drawImage (imagen, 0, 0, image.width, image.height, 0, 0, size.width, size.height);
    volver lienzo;
};

funci�n hasEntries (array) {
    return array.length> 0;
}

}). call ({}, typeof (window)! == "undefined"? window: undefined, typeof (document)! == "undefined"? document: undefined);