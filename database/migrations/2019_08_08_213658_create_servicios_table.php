<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
             $table->uuid('id')->primary();
            $table->string('nombre_cliente')->unique();
            $table->string('domicilio');
            $table->string('tel_local');
            $table->string('tel_celular');
            $table->string('nombre_contacto');
            $table->string('comenrarios');
            $table->integer('servicio_tecnico');
            $table->integer('televisiones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios');
    }
}
